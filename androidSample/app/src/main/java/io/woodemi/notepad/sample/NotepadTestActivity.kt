package io.woodemi.notepad.sample

import android.os.Bundle
import android.support.v7.app.AppCompatActivity
import io.woodemi.notepad.*
import org.jetbrains.anko.*
import org.jetbrains.anko.sdk27.coroutines.onClick

class NotepadTestActivity : AppCompatActivity() {
    private val notepadScanResult by lazy {
        intent.getParcelableExtra<NotepadScanResult>(MainActivity.EXTRA_SCAN_RESULT)
    }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        NotepadConnector.callback = connectorCallback
        scrollView {
            verticalLayout {
                contentLayout()
            }
        }
    }

    override fun onDestroy() {
        super.onDestroy()
        NotepadConnector.callback = null
        notepadClient.callback = null
    }

    private fun @AnkoViewDslMarker _LinearLayout.contentLayout() {
        button("connect").onClick {
            NotepadConnector.connect(it!!.context, notepadScanResult, byteArrayOf(0, 0, 0, 2))
        }
        button("disconnect").onClick {
            NotepadConnector.disconnect()
        }
        button("import all").onClick {
            notepadClient.getMemoSummary({
                importMemoRecursively(it.memoCount)
            }, errorToaster("getMemoSummary"))
        }
    }

    private lateinit var notepadClient: NotepadClient

    private val connectorCallback = object : NotepadConnectorCallback {
        override fun onConnectionStateChange(notepadClient: NotepadClient, state: ConnectionState) {
            runOnUiThread {
                val cause = if (state is ConnectionState.Disconnected) " ${state.cause}" else ""
                toast(state.javaClass.simpleName + cause)
            }
            if (state is ConnectionState.Connected) {
                this@NotepadTestActivity.notepadClient = notepadClient
                notepadClient.callback = this@NotepadTestActivity.clientCallback
            } else if (state is ConnectionState.Disconnected) {
                notepadClient.callback = null
            }
        }
    }

    private val clientCallback = object : NotepadClient.Callback {
        override fun handlePointer(list: List<NotePenPointer>) {
            println("handlePointer ${list.size}")
        }

        override fun handleEvent(message: NotepadMessage) {
            println("handleEvent $message")
        }
    }

    private fun importMemoRecursively(restCount: Int) {
        if (restCount == 0) {
            return runOnUiThread { toast("import all complete") }
        }

        notepadClient.importMemo({
            println("importMemo progress $it")
        }, {
            notepadClient.deleteMemo({
                importMemoRecursively(restCount - 1)
            }, errorToaster("deleteMemo"))
        }, errorToaster("importMemo"))
    }
}