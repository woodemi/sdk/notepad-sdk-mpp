//
// Created by SunbreakWang on 2019-04-19.
// Copyright (c) 2019 Woodemi Tech Co., Ltd. All rights reserved.
//

import Foundation

extension NSObjectProtocol {
    func apply(_ closure: (Self) -> Void) -> Self {
        closure(self)
        return self
    }
}
