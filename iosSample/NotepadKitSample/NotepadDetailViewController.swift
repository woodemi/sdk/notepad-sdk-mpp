//
// Created by SunbreakWang on 2019-04-24.
// Copyright (c) 2019 Woodemi Tech Co., Ltd. All rights reserved.
//

import UIKit
import NotepadKit

class NotepadDetailViewController: UIViewController {
    let notepadConnector = NotepadConnector()
    var notepadScanResult: NotepadScanResult!
    var notepadClient: NotepadClient!

    override func viewDidLoad() {
        super.viewDidLoad()
        notepadConnector.connectionDelegate = self

        view.layoutMargins = UIEdgeInsets(top: 20, left: 0, bottom: 20, right: 0)
        let scrollView = view.layout(subView: UIScrollView()) {
            $0.matchParent()
        }
        scrollView.layout(subView: UIStackView()) { stackView in
            stackView.matchParent()
            stackView.axis = .vertical
            self.btnConnect = self.addArrangedButton(to: stackView, withTitle: "connect")
            self.btnDisconnect = self.addArrangedButton(to: stackView, withTitle: "disconnect")
            self.btnClaimAuth = self.addArrangedButton(to: stackView, withTitle: "claimAuth")
            self.btnDisclaimAuth = self.addArrangedButton(to: stackView, withTitle: "disclaimAuth")
            self.btnSetMode = self.addArrangedButton(to: stackView, withTitle: "setMode")
            self.btnGetMemoSummary = self.addArrangedButton(to: stackView, withTitle: "getMemoSummary")
            self.btnGetMemoInfo = self.addArrangedButton(to: stackView, withTitle: "getMemoInfo")
            self.btnImportMemo = self.addArrangedButton(to: stackView, withTitle: "importMemo")
            self.btnDeleteMemo = self.addArrangedButton(to: stackView, withTitle: "deleteMemo")
            self.btnGetDeviceName = self.addArrangedButton(to: stackView, withTitle: "getDeviceName")
            self.btnSetDeviceName = self.addArrangedButton(to: stackView, withTitle: "setDeviceName")
            self.btnGetAutoLockTime = self.addArrangedButton(to: stackView, withTitle: "getAutoLockTime")
            self.btnSetAutoLockTime = self.addArrangedButton(to: stackView, withTitle: "setAutoLockTime")
            self.btnGetDeviceVersion = self.addArrangedButton(to: stackView, withTitle: "getDeviceVersion")
            self.btnSetDeviceVersion  = self.addArrangedButton(to: stackView, withTitle: "setDeviceVersion")
            self.btnGetSize = self.addArrangedButton(to: stackView, withTitle: "getGetSize")
        }
    }
    
    override func viewDidDisappear(_ animated: Bool) {
        notepadConnector.connectionDelegate = nil
        notepadClient.callback = nil
    }

    func addArrangedButton(to stackView: UIStackView, withTitle titleText: String) -> UIButton {
        return stackView.arrangedLayout(subView: UIButton()) {
            $0.setTitle(titleText, for: .normal)
            $0.setTitleColor(.blue, for: .normal)
            $0.heightAnchor.constraint(equalToConstant: 40)
            $0.addTarget(self, action: #selector(self.buttonDidClick), for: .touchUpInside)
        }
    }

    var btnConnect: UIButton!
    var btnDisconnect: UIButton!
    var btnClaimAuth: UIButton!
    var btnDisclaimAuth: UIButton!
    var btnSetMode: UIButton!
    var btnGetMemoSummary: UIButton!
    var btnGetMemoInfo: UIButton!
    var btnImportMemo: UIButton!
    var btnDeleteMemo: UIButton!
    var btnGetDeviceName: UIButton!
    var btnSetDeviceName: UIButton!
    var btnGetBatteryInfo: UIButton!
    var btnGetDeviceDate: UIButton!
    var btnSetDeviceDate: UIButton!
    var btnGetAutoLockTime: UIButton!
    var btnSetAutoLockTime: UIButton!
    var btnGetDeviceVersion: UIButton!
    var btnSetDeviceVersion: UIButton!
    var btnGetSize: UIButton!

    @objc func buttonDidClick(_ sender: UIButton) {
        print("buttonDidClick \(sender)")
        switch (sender) {
        case btnConnect:
            let authToken = Data(bytes: [0x00, 0x00, 0x00, 0x02])
            notepadConnector.connect(to: notepadScanResult, authToken: NSData_ExtensionsKt.toByteArray(authToken))
        case btnDisconnect:
            notepadConnector.disconnect()
        case btnClaimAuth:
            notepadClient?.claimAuth(complete: {
                self.toast("claimAuth complete")
            }) {
                self.toast("claimAuth error \($0.description)")
            }
        case btnDisclaimAuth:
            notepadClient?.disclaimAuth(complete: {
                self.toast("disclaimAuth complete")
            }) {
                self.toast("disclaimAuth error \($0.description)")
            }
        case btnSetMode:
            notepadClient?.setMode(to: .sync, complete: {
                self.toast("setMode complete")
            }) {
                self.toast("setMode error \($0.description)")
            }
        case btnGetMemoSummary:
            notepadClient?.getMemoSummary(success: {
                self.toast("getMemoSummary success \($0.description)")
            }) {
                self.toast("getMemoSummary error \($0.description)")
            }
        case btnGetMemoInfo:
            notepadClient?.getMemoInfo(success: {
                self.toast("getMemoInfo success \($0.description)")
            }) {
                self.toast("getMemoInfo error \($0.description)")
            }
        case btnImportMemo:
            notepadClient?.importMemo(progress: {
                print("importMemo progress \($0)")
            }, success: {
                self.toast("importMemo success \($0.description)")
            }) {
                self.toast("importMemo error \($0.description)")
            }
        case btnDeleteMemo:
            notepadClient?.deleteMemo(complete: {
                self.toast("deleteMemo complete")
            }) {
                self.toast("deleteMemo error \($0.description)")
            }
        case btnGetDeviceName:
            notepadClient?.getDeviceName(success: {
                self.toast("getDeviceName success \($0.description)")
            }) {
                self.toast("getDeviceName error \($0.description)")
            }
        case btnSetDeviceName:
            let remainder = Int(Date().timeIntervalSince1970) % 10
            notepadClient?.setDeviceName(to: "test\(remainder)", complete: {
                self.toast("setDeviceName complete")
            }) {
                self.toast("setDeviceName error \($0.description)")
            }
        case btnGetBatteryInfo:
            notepadClient?.getBatteryInfo(success: {
                self.toast("getBatteryInfo success \($0.description)")
            }) {
                self.toast("getBatteryInfo error \($0.description)")
            }
        case btnGetDeviceDate:
            notepadClient?.getDeviceDate(success: {
                self.toast("getDeviceDate success \($0.description)")
            }) {
                self.toast("getDeviceDate error \($0.description)")
            }
        case btnSetDeviceDate:
            let oneHourLater = Date().addingTimeInterval(3600).timeIntervalSince1970 * 1000
            notepadClient?.setDeviceDate(to: numericCast(Int(oneHourLater)), complete: {
                self.toast("setDeviceDate complete")
            }) {
                self.toast("setDeviceDate error \($0.description)")
            }
        case btnGetAutoLockTime:
            notepadClient?.getAutoLockTime(success: {
                self.toast("getAutoLockTime success \($0.description)")
            }) {
                self.toast("getAutoLockTime error \($0.description)")
            }
        case btnSetAutoLockTime:
            let oneHour = TimeInterval(3600)
            notepadClient?.setAutoLockTime(to: numericCast(Int(oneHour)), complete: {
                self.toast("setAutoLockTime complete")
            }) {
                self.toast("setAutoLockTime error \($0.description)")
            }
        case btnGetDeviceVersion:
            notepadClient?.getVersionInfo(success: {
                var version = String(format: "%ld", Int($0.software.major))
                if let minor = $0.software.minor {
                    version = version + "."
                    version = version + String(format: "%ld", Int(minor))
                }
                if let patch = $0.software.patch {
                    version = version + "."
                    version = version + String(format: "%ld", Int(patch))
                }
                self.toast("setAutoLockTime complete version=\(version)")
            }) {
                self.toast("btnGetDeviceVersion error \($0.description)")
            }
        case btnSetDeviceVersion:
            //  告诉device要安装的"版本"
            let newestVersion = "255.255.255"
            
            //  实际给device的版本（1.1.1版本）
            guard let filePath = getVersionPath(versionName: "1.1.1") else {
                return
            }
            
            let versionArr = newestVersion.components(separatedBy: ".")
            guard versionArr.count > 0, let major = Int32(versionArr[0]) else {
                return
            }
            
            var minor: KotlinInt? = nil
            if versionArr.count > 1, let v2 = Int32(versionArr[1]) {
                minor = KotlinInt(int: v2)
            }
            var patch: KotlinInt? = nil
            if versionArr.count > 2, let v3 = Int32(versionArr[2]) {
                patch = KotlinInt(int: v3)
            }
            
            let targetVersion = Version(major: major, minor: minor, patch: patch)   //  新版本号
            notepadClient?.upgrade(with: filePath, version: targetVersion, progress: {
                print("Upgrading onNext: \($0)")
            }, complete: {
                self.toast("Upgrading onCompleted")
            }) {
                self.toast("btnGetDeviceVersion error \($0.description)")
            }
        case btnGetSize:
            self.toast("W: \(notepadClient.width), H: \(notepadClient.height)")
        default:
            print("Unknown buttonDidClick \(sender)")
        }
    }
}

extension NotepadDetailViewController: NotepadConnectionDelegate {
    func willConnect(notepadClient: NotepadClient) {
        toast("Connecting")
        self.notepadClient = notepadClient
    }
    
    func awaitConfirm(notepadClient: NotepadClient) {
        toast("AwaitConfirm")
    }

    func didConnect(notepadClient: NotepadClient) {
        notepadClient.callback = self
        toast("Connected")
    }
    
    func didDisconnect(notepadClient: NotepadClient, cause: DisconnectionCause) {
        notepadClient.callback = nil
        toast("Disconnected \(cause)")
    }
}

extension NotepadDetailViewController: NotepadClientCallback {
    func handlePointer(list: [NotePenPointer]) {
        print("handlePointer \(list.count)")
    }
    
    func handleEvent(message: NotepadMessage) {
        print("handleEvent \(message)")
    }
}

/**
 *  OTA包存储路径
 *  等价于：FileManager.default.pathForFile(inDocumentDirectory: "\(versionName)")
 */
private func getVersionPath(versionName: String) ->  String? {
    guard versionName.count > 0 else {
        return nil
    }
    let documentPath = NSHomeDirectory() + "/Documents"
    let path = documentPath + "/" + versionName + ".serc"
    return path
}
