//
// Created by sunbreak on 2019/8/30.
// Copyright (c) 2019 Woodemi Tech Co., Ltd. All rights reserved.
//

import UIKit
import NotepadKit

class NotepadTestViewController: UIViewController {
    let notepadConnector = NotepadConnector()
    var notepadScanResult: NotepadScanResult!
    var notepadClient: NotepadClient!

    override func viewDidLoad() {
        super.viewDidLoad()
        view.layoutMargins = UIEdgeInsets(top: 20, left: 0, bottom: 20, right: 0)
        let scrollView = view.layout(subView: UIScrollView()) {
            $0.matchParent()
        }
        scrollView.layout(subView: UIStackView()) { stackView in
            stackView.matchParent()
            stackView.axis = .vertical
            self.btnConnect = self.addArrangedButton(to: stackView, withTitle: "connect")
            self.btnDisconnect = self.addArrangedButton(to: stackView, withTitle: "disconnect")
            self.btnImportAll = self.addArrangedButton(to: stackView, withTitle: "import all")
        }
    }

    func addArrangedButton(to stackView: UIStackView, withTitle titleText: String) -> UIButton {
        return stackView.arrangedLayout(subView: UIButton()) {
            $0.setTitle(titleText, for: .normal)
            $0.setTitleColor(.blue, for: .normal)
            $0.heightAnchor.constraint(equalToConstant: 40)
            $0.addTarget(self, action: #selector(self.buttonDidClick), for: .touchUpInside)
        }
    }

    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        notepadConnector.connectionDelegate = self
    }

    override func viewDidDisappear(_ animated: Bool) {
        notepadConnector.connectionDelegate = nil
        notepadClient.callback = nil
    }

    var btnConnect: UIButton!
    var btnDisconnect: UIButton!
    var btnImportAll: UIButton!

    @objc func buttonDidClick(_ sender: UIButton) {
        print("buttonDidClick \(sender)")
        switch (sender) {
        case btnConnect:
            let authToken = Data(bytes: [0x00, 0x00, 0x00, 0x02])
            notepadConnector.connect(to: notepadScanResult, authToken: NSData_ExtensionsKt.toByteArray(authToken))
        case btnDisconnect:
            notepadConnector.disconnect()
        case btnImportAll:
            notepadClient?.getMemoSummary(success: {
                self.importMemoRecursively(Int($0.memoCount))
            }, error: errorToaster("getMemoSummary"))
        default:
            print("Unknown buttonDidClick \(sender)")
        }
    }
}

extension NotepadTestViewController: NotepadConnectionDelegate {
    func willConnect(notepadClient: NotepadClient) {
        toast("Connecting")
        self.notepadClient = notepadClient
    }

    func awaitConfirm(notepadClient: NotepadClient) {
        toast("AwaitConfirm")
    }

    func didConnect(notepadClient: NotepadClient) {
        notepadClient.callback = self
        toast("Connected")
    }

    func didDisconnect(notepadClient: NotepadClient, cause: DisconnectionCause) {
        notepadClient.callback = nil
        toast("Disconnected \(cause)")
    }
}

extension NotepadTestViewController: NotepadClientCallback {
    func handlePointer(list: [NotePenPointer]) {
        print("handlePointer \(list.count)")
    }

    func handleEvent(message: NotepadMessage) {
        print("handleEvent \(message)")
    }
}

extension NotepadTestViewController {
    func importMemoRecursively(_ restCount: Int) {
        if (restCount == 0) {
            return toast("import all complete")
        }

        notepadClient?.importMemo(progress: {
            print("importMemo progress \($0)")
        }, success: { [weak self] _ in
            guard let s = self else {
                return
            }
            s.notepadClient?.deleteMemo(complete: {
                s.importMemoRecursively(restCount - 1)
            }, error: s.errorToaster("deleteMemo"))
        }, error: errorToaster("importMemo"))
    }
}
