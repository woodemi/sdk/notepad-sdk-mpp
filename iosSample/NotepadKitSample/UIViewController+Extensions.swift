//
// Created by SunbreakWang on 2019-04-26.
// Copyright (c) 2019 Woodemi Tech Co., Ltd. All rights reserved.
//

import UIKit
import NotepadKit

extension UIViewController {
    func toast(_ message: String) {
        let alertController = UIAlertController(title: nil, message: message, preferredStyle: .alert)
        present(alertController, animated: false)
        DispatchQueue.main.asyncAfter(deadline: .now() + 2) {
            alertController.dismiss(animated: false)
        }
    }

    func errorToaster(_ tag: String) -> ((Failure) -> ()) {
        return { [weak self] (failure) -> () in
            let message = "\(tag) error \(failure)"
            print(message)
            self?.toast(message)
        }
    }
}
