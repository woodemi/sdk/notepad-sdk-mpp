//
//  ViewController.swift
//  NotepadKitSample
//
//  Created by SunbreakWang on 2019/1/19.
//  Copyright © 2019 Woodemi Tech Co., Ltd. All rights reserved.
//

import UIKit
import NotepadKit
import CoreBluetooth

class ViewController: UIViewController {
    let notepadConnector = NotepadConnector()
    let connectorReady = DispatchGroup()

    var tableView: UITableView!

    override func viewDidLoad() {
        super.viewDidLoad()
        notepadConnector.scanDelegate = self
        connectorReady.enter()

        title = Bundle.main.infoDictionary?["CFBundleName"] as? String

        tableView = view.layout(subView: UITableView()) {
            $0.frame = self.view.bounds
            $0.dataSource = self
            $0.delegate = self
        }
    }

    var notepadScanResults = Array<NotepadScanResult>()

    override func viewWillAppear(_ animated: Bool) {
        connectorReady.notify(queue: .main) { self.notepadConnector.startScan() }
    }

    override func viewDidDisappear(_ animated: Bool) {
        notepadConnector.stopScan()
    }
}

extension ViewController: NotepadScanDelegate {
    func didChange(state: Bool) {
        if state {
            connectorReady.leave()
        }
    }
    
    func didDiscover(notepadScanResult: NotepadScanResult) {
        print("ViewController didDiscover \(notepadScanResult.deviceId)")
        notepadScanResults.append(notepadScanResult)
        tableView.reloadData()
    }
}

extension ViewController: UITableViewDataSource {
    public func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return notepadScanResults.count
    }

    public func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let notepadScanResult = notepadScanResults[indexPath.row]
        return UITableViewCell(style: .subtitle, reuseIdentifier: "UITableViewCell").apply {
            $0.textLabel?.text = "\(notepadScanResult.name ?? "nil")(\(notepadScanResult.rssi))"
            $0.detailTextLabel?.text = "\(notepadScanResult.deviceId)"
        }
    }
}

extension ViewController: UITableViewDelegate {
    public func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        navigationController?.pushViewController(NotepadTestViewController().apply {
            $0.edgesForExtendedLayout = []
            $0.notepadScanResult = notepadScanResults[indexPath.row]
        }, animated: true)
    }
}
