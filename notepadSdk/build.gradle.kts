import org.jetbrains.kotlin.gradle.plugin.mpp.Framework
import org.jetbrains.kotlin.gradle.plugin.mpp.KotlinAndroidTarget
import org.jetbrains.kotlin.gradle.plugin.mpp.KotlinNativeTarget

plugins {
    val kotlinVersion = "1.3.40"

    id("kotlin-multiplatform") version kotlinVersion
    id("com.android.library") version "3.2.0"
    id("kotlin-android-extensions") version kotlinVersion
    id("maven-publish")
}

repositories {
    google()
    jcenter()
}

group = "io.woodemi"
version = "2.4.0"

// Normally [RELEASE, DEBUG]
val iOSBuildType = project.properties["kotlin.build.type"] as? String ?: "DEBUG"
val embedBitcodMode = if (iOSBuildType == "RELEASE") Framework.BitcodeEmbeddingMode.BITCODE else Framework.BitcodeEmbeddingMode.MARKER
// [iosX64, iosArm64, iosArm32]
val iOSTargetPreset = project.properties["kotlin.target"] as? String ?: "iosX64"

kotlin {
    targets {
        targetFromPreset(presets.getByName("android"), "androidLib") {
            (this as KotlinAndroidTarget).publishLibraryVariants("release")
        }

        targetFromPreset(presets.getByName(iOSTargetPreset), "ios") {
            (this as KotlinNativeTarget).binaries.framework {
                compilations["main"].extraOpts("-module-name", "NK")
                embedBitcode(embedBitcodMode)
            }
        }
    }

    sourceSets {
        val coroutineVersion = "1.2.2"
        val serialization_version = "0.11.1"
        val commonMain by getting {
            dependencies {
                implementation("org.jetbrains.kotlin:kotlin-stdlib-common")
                implementation("org.jetbrains.kotlinx:kotlinx-coroutines-core-common:$coroutineVersion")
                implementation("org.jetbrains.kotlinx:kotlinx-serialization-runtime-common:$serialization_version")
            }
        }
        val androidLibMain by getting {
            dependencies {
                implementation("org.jetbrains.kotlin:kotlin-stdlib")
                implementation("org.jetbrains.kotlinx:kotlinx-coroutines-android:$coroutineVersion")
                implementation("org.jetbrains.kotlinx:kotlinx-serialization-runtime:$serialization_version")
            }
        }
        val iosMain by getting {
            dependencies {
                implementation("org.jetbrains.kotlinx:kotlinx-coroutines-core-native:$coroutineVersion")
                implementation("org.jetbrains.kotlinx:kotlinx-serialization-runtime-native:$serialization_version")
            }
        }
    }
}

android {
    compileSdkVersion(28)
    defaultConfig {
        minSdkVersion(19)
        targetSdkVersion(28)
        versionCode = 200
        versionName = version.toString()
        testInstrumentationRunner = "android.support.test.runner.AndroidJUnitRunner"
    }
    buildTypes {
        val release by getting {
            isMinifyEnabled = false
        }
    }
}

androidExtensions {
    isExperimental = true
}

// This task attaches native framework built from ios module to Xcode project
// (see iosApp directory). Don't run this task directly,
// Xcode runs this task itself during its build process.
// Before opening the project from iosApp directory in Xcode,
// make sure all Gradle infrastructure exists (gradle.wrapper, gradlew).
tasks.create("copyFramework") {
    val nativeTarget = kotlin.targets["ios"] as KotlinNativeTarget
    val nativeBinary = nativeTarget.binaries.getFramework(iOSBuildType)
    dependsOn(nativeBinary.linkTask)

    doLast {
        val targetDir = project.property("configuration.build.dir") as? String ?: "."
        copy {
            val outputFile = nativeBinary.outputFile
            from(outputFile.parent)
            into(targetDir)
            include("${outputFile.name}/**")
            include("${outputFile.name}.dSYM")
        }
    }
}

publishing.repositories {
    maven {
        url = uri("$buildDir/repo")
    }
}
