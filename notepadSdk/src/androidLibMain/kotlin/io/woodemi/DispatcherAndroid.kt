package io.woodemi

import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.delay

/**
 * Created by wangkun on 2019-04-28.
 */
internal actual val ApplicationDispatcher = Dispatchers.Default

internal actual suspend fun dispatchAfter(timeMillis: Long, block: () -> Unit) {
    delay(timeMillis)
    block()
}