package io.woodemi

actual class Failure actual constructor(
        e: Exception
) {
    val exception = java.lang.Exception(e.message, e.cause)
}