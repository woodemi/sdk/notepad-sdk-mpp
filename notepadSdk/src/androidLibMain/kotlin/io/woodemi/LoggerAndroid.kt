package io.woodemi

import android.util.Log

/**
 * Created by wangkun on 2019-05-09.
 */
actual object Logger {
    actual var isEnabled = true

    /**
     * Verbose
     */
    actual fun v(tag: String, message: String) {
        if (!isEnabled) return
        Log.v(tag, message)
    }

    /**
     * Debug
     */
    actual fun d(tag: String, message: String) {
        if (!isEnabled) return
        Log.d(tag, message)
    }

    /**
     * Info
     */
    actual fun i(tag: String, message: String) {
        if (!isEnabled) return
        Log.i(tag, message)
    }

    /**
     * Warning
     */
    actual fun w(tag: String, message: String) {
        if (!isEnabled) return
        Log.w(tag, message)
    }

    /**
     * Error
     */
    actual fun e(tag: String, message: String) {
        if (!isEnabled) return
        android.util.Log.e(tag, message)
    }

    /**
     * Severe
     */
    actual fun s(tag: String, message: String) {
        Log.wtf(tag, message)
    }
}