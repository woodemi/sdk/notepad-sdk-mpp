package io.woodemi

import java.io.FileInputStream

/**
 * Created by wangkun on 2019-06-27.
 */
internal actual fun readUTF8Text(path: String): String = FileInputStream(path).use {
    it.reader(Charsets.UTF_8).readText()
}