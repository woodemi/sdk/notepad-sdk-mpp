package io.woodemi.notepad

/**
 * Created by wangkun on 2018-11-30.
 */
sealed class ConnectionState {
    class Disconnected(_cause: DisconnectionCause = DisconnectionCause.Unknown) : ConnectionState() {
        val cause = _cause
    }

    object Connecting : ConnectionState()

    object AwaitConfirm : ConnectionState()

    object Connected : ConnectionState()
}