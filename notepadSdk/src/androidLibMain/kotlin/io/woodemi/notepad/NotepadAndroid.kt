package io.woodemi.notepad

import android.bluetooth.le.ScanResult
import android.os.Parcelable
import io.woodemi.toByteArray
import kotlinx.android.parcel.Parcelize

/**
 * Created by wangkun on 2019/4/17.
 */
@Parcelize
actual data class NotepadScanResult(
    actual val name: String?,
    actual val deviceId: String,
    actual val manufacturerData: ByteArray?,
    actual val rssi: Int
) : Parcelable

val ScanResult.manufacturerData: ByteArray?
    get() {
        val sparseArray = scanRecord.manufacturerSpecificData ?: return null
        if (sparseArray.size() == 0) return null

        return sparseArray.keyAt(0).toShort().toByteArray() + sparseArray.valueAt(0)
    }