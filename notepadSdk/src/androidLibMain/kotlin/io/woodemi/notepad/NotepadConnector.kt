package io.woodemi.notepad

import android.bluetooth.*
import android.content.Context
import io.woodemi.Logger
import kotlinx.coroutines.GlobalScope
import kotlinx.coroutines.channels.BroadcastChannel
import kotlinx.coroutines.channels.Channel
import kotlinx.coroutines.flow.asFlow
import kotlinx.coroutines.flow.filter
import kotlinx.coroutines.flow.single
import kotlinx.coroutines.flow.take
import kotlinx.coroutines.launch

private const val TAG = "NotepadConnector"

interface NotepadConnectorCallback {
    fun onConnectionStateChange(notepadClient: NotepadClient, state: ConnectionState)
}

object NotepadConnector {
    private var notepadClient: NotepadClient? = null
    @JvmField
    var callback: NotepadConnectorCallback? = null

    @JvmOverloads
    fun connect(context: Context, scanResult: NotepadScanResult, authToken: ByteArray? = null) {
        Logger.i(TAG, "connect")
        notepadClient = NotepadHelper.create(scanResult.manufacturerData, authToken)
        connectGatt = with(context) {
            val bluetoothManager = getSystemService(Context.BLUETOOTH_SERVICE) as BluetoothManager
            val bluetoothDevice = bluetoothManager.adapter.getRemoteDevice(scanResult.deviceId)
            bluetoothDevice.connectGatt(context, false, gattCallback)
        }
        notepadType = NotepadType(notepadClient!!, BleType(connectGatt!!))

        callback?.onConnectionStateChange(notepadClient!!, ConnectionState.Connecting)
    }

    fun disconnect() {
        Logger.i(TAG, "disconnect")
        val gatt = connectGatt ?: return
        GlobalScope.launch {
            gatt.disconnect()
            connectionStateChannel.asFlow().filter { it is ConnectionState.Disconnected }.take(1).single()
            gatt.close()
        }
    }

    private var connectGatt: BluetoothGatt? = null

    private var notepadType: NotepadType? = null

    private var connectionStateChannel = BroadcastChannel<ConnectionState>(Channel.CONFLATED).also { channel ->
        // TODO Cancel subscription
        GlobalScope.launch {
            for (state in channel.openSubscription()) {
                val client = notepadClient ?: continue
                callback?.onConnectionStateChange(client, state)
            }
        }
    }

    private val gattCallback = object : BluetoothGattCallback() {
        override fun onConnectionStateChange(gatt: BluetoothGatt?, status: Int, newState: Int) {
            if (gatt != connectGatt) return
            Logger.v(TAG, "onConnectionStateChange: status($status), newState($newState)")

            if (newState == BluetoothGatt.STATE_CONNECTED && status == BluetoothGatt.GATT_SUCCESS) {
                connectGatt?.discoverServices()
            } else {
                GlobalScope.launch { connectionStateChannel.send(ConnectionState.Disconnected()) }
            }
        }

        override fun onServicesDiscovered(gatt: BluetoothGatt?, status: Int) {
            if (gatt != connectGatt || status != BluetoothGatt.GATT_SUCCESS) return
            gatt?.logCharacteristics()

            val notepadType = notepadType ?: return
            GlobalScope.launch {
                try {
                    notepadType.configCharacteristics()
                    notepadType.notepadClient.completeConnection { awaitConfirm ->
                        if (awaitConfirm) GlobalScope.launch { connectionStateChannel.send(ConnectionState.AwaitConfirm) }
                    }
                    connectionStateChannel.send(ConnectionState.Connected)
                } catch (e: AccessException) {
                    // FIXME On some mobile phone [BluetoothGatt.STATE_DISCONNECTED] prioritize [AccessException.Unconfirmed]
                    val disconnectionCause = when (e) {
                        AccessException.Denied -> DisconnectionCause.Unauthorized
                        AccessException.Unconfirmed -> DisconnectionCause.Unconfirmed
                    }
                    connectionStateChannel.send(ConnectionState.Disconnected(disconnectionCause))
                } catch (e: Exception) {
                    connectionStateChannel.send(ConnectionState.Disconnected())
                }
            }
        }

        override fun onMtuChanged(gatt: BluetoothGatt?, mtu: Int, status: Int) {
            if (gatt != connectGatt || status != BluetoothGatt.GATT_SUCCESS) return
            Logger.v(TAG, "onMtuChanged $mtu")
            GlobalScope.launch { notepadType?.mtuConfigChannel?.send(mtu) }
        }

        override fun onDescriptorWrite(gatt: BluetoothGatt?, descriptor: BluetoothGattDescriptor, status: Int) {
            if (gatt != connectGatt) return
            Logger.v(TAG, "onDescriptorWrite ${descriptor.uuid}, ${descriptor.characteristic.uuid}, $status")
            val serviceCharacteristic = descriptor.characteristic.serviceCharacteristic
            GlobalScope.launch { notepadType?.characteristicConfigChannel?.send(serviceCharacteristic) }
        }

        override fun onCharacteristicRead(gatt: BluetoothGatt?, characteristic: BluetoothGattCharacteristic, status: Int) {
            if (gatt != connectGatt) return
            val readValue = characteristic.characteristicValue
            Logger.v(TAG, "onCharacteristicRead ${readValue.first}, ${readValue.second.contentToString()} $status")
            GlobalScope.launch { notepadType?.inputValueChannel?.send(readValue) }
        }

        override fun onCharacteristicWrite(
                gatt: BluetoothGatt?,
                characteristic: BluetoothGattCharacteristic,
                status: Int
        ) {
            if (gatt != connectGatt) return
            val writeValue = characteristic.characteristicValue
            Logger.v(TAG, "onCharacteristicWrite ${writeValue.first}, ${writeValue.second.contentToString()} $status")
            GlobalScope.launch { notepadType?.outputValueChannel?.send(writeValue) }
        }

        override fun onCharacteristicChanged(gatt: BluetoothGatt?, characteristic: BluetoothGattCharacteristic) {
            if (gatt != connectGatt) return
            val valueChange = characteristic.characteristicValue
            Logger.v(TAG, "onCharacteristicChanged ${valueChange.first}, ${valueChange.second.contentToString()}")
            GlobalScope.launch { notepadType?.inputValueChannel?.send(valueChange) }
        }
    }

}

private val BluetoothGattCharacteristic.serviceCharacteristic
    get() = service.uuid.uuidString to uuid.uuidString

private val BluetoothGattCharacteristic.characteristicValue
    get() = uuid.uuidString to value

private fun BluetoothGatt.logCharacteristics() = services.forEach { service ->
    Logger.v(TAG, "Service " + service.uuid)
    service.characteristics.forEach { characteristic ->
        Logger.v(TAG, "    Characteristic ${characteristic.uuid}")
        characteristic.descriptors.forEach {
            Logger.v(TAG, "        Descriptor ${it.uuid}")
        }
    }
}