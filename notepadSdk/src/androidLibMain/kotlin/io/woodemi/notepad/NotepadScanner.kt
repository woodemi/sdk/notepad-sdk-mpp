package io.woodemi.notepad

import android.annotation.TargetApi
import android.bluetooth.BluetoothAdapter
import android.bluetooth.BluetoothManager
import android.bluetooth.le.ScanCallback
import android.bluetooth.le.ScanResult
import android.content.Context
import android.os.Build
import android.util.Log
import io.woodemi.Logger
import io.woodemi.toByteBuffer
import io.woodemi.uint8
import kotlinx.io.ByteBuffer

/**
 * Created by wangkun on 05/03/2018.
 */
private const val TAG = "NotepadScanner"

class NotepadScanner(context: Context) {

    interface Callback {
        fun onScanResult(result: NotepadScanResult)
    }

    private val notepadScannerImpl: NotepadScannerInterface

    init {
        val bluetoothManager = context.getSystemService(Context.BLUETOOTH_SERVICE) as BluetoothManager
        notepadScannerImpl = if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP)
            LollipopNotepadScanner(bluetoothManager.adapter) else LegacyNotepadScanner(bluetoothManager.adapter)
    }

    var callback: Callback?
        get() = notepadScannerImpl.callback
        set(value) {
            notepadScannerImpl.callback = value
        }

    fun startScan() {
        Logger.i(TAG, "startScan")
        notepadScannerImpl.startScan()
    }

    fun stopScan() {
        Logger.i(TAG, "stopScan")
        notepadScannerImpl.stopScan()
    }
}

private interface NotepadScannerInterface {
    var callback: NotepadScanner.Callback?

    fun startScan()

    fun stopScan()
}

@TargetApi(Build.VERSION_CODES.LOLLIPOP)
private class LollipopNotepadScanner(val adapter: BluetoothAdapter) : NotepadScannerInterface {
    private var _callback: NotepadScanner.Callback? = null
    override var callback: NotepadScanner.Callback?
        get() = _callback
        set(value) {
            _callback = value
        }

    override fun startScan() {
        adapter.bluetoothLeScanner.startScan(scanCallback)
    }

    override fun stopScan() {
        adapter.bluetoothLeScanner.stopScan(scanCallback)
    }

    val scanCallback = object : ScanCallback() {
        override fun onScanFailed(errorCode: Int) {
            Log.v(TAG, "onScanFailed: $errorCode")
        }

        override fun onScanResult(callbackType: Int, result: ScanResult) {
            Log.v(TAG, "onScanResult: $callbackType + $result")
            val notepadScanResult = NotepadScanResult(result.device.name, result.device.address, result.manufacturerData, result.rssi)
            if (NotepadHelper.support(notepadScanResult.manufacturerData)) {
                callback?.onScanResult(notepadScanResult)
            }
        }

        override fun onBatchScanResults(results: MutableList<ScanResult>?) {
            Log.v(TAG, "onBatchScanResults: $results")
        }
    }

}

private class LegacyNotepadScanner(val adapter: BluetoothAdapter) : NotepadScannerInterface {
    private var _callback: NotepadScanner.Callback? = null
    override var callback: NotepadScanner.Callback?
        get() = _callback
        set(value) {
            _callback = value
        }

    override fun startScan() {
        adapter.startLeScan(leScanCallback)
    }

    override fun stopScan() {
        adapter.stopLeScan(leScanCallback)
    }

    private val leScanCallback = BluetoothAdapter.LeScanCallback { device, rssi, scanRecord ->
        Log.v(TAG, "onLeScan: name(${device.name}), address(${device.address}), rssi(${rssi})")
        val manufacturerData = BleTagValue.findByTag(scanRecord, BleTagValue.TAG_ADV_MANUFACTURER_DATA)?.value
        val notepadScanResult = NotepadScanResult(device.name, device.address, manufacturerData, rssi)
        if (NotepadHelper.support(notepadScanResult.manufacturerData)) {
            callback?.onScanResult(notepadScanResult)
        }
    }
}

private class BleTagValue(val tag: Byte, val value: ByteArray) {
    companion object {
        const val TAG_ADV_MANUFACTURER_DATA = 0xFF.toByte()

        fun findByTag(scanRecord: ByteArray, tag: Byte): BleTagValue? {
            val buffer = scanRecord.toByteBuffer()
            var length = buffer.uint8.toInt()
            while (length > 0) {
                val bleTagValue = pop(buffer, length)
                if (bleTagValue.tag == tag) return bleTagValue
                length = buffer.uint8.toInt()
            }
            return null
        }

        fun pop(buffer: ByteBuffer, length: Int): BleTagValue {
            val tag = buffer.uint8
            val value = ByteArray(length - 1).apply { buffer.get(this, 0, this.size) }
            return BleTagValue(tag.toByte(), value)
        }
    }
}