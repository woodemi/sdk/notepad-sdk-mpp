package io.woodemi.notepad

import android.bluetooth.BluetoothGatt
import android.bluetooth.BluetoothGatt.*
import android.bluetooth.BluetoothGattDescriptor
import android.os.Build
import kotlinx.coroutines.GlobalScope
import kotlinx.coroutines.channels.Channel
import kotlinx.coroutines.launch
import java.util.*

/**
 * Created by wangkun on 2019-04-26.
 */
private val DESC__CLIENT_CHAR_CONFIGURATION = UUID.fromString("00002902-0000-1000-8000-00805f9b34fb")

internal actual class BleType(private val gatt: BluetoothGatt) {
    actual val mtuConfigChannel: Channel<Int> = Channel()

    actual fun requestMtu(expectedMtu: Int) {
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
            gatt.requestMtu(expectedMtu)
        } else {
            // FIXME https://punchthrough.com/maximizing-ble-throughput-on-ios-and-android/
            // Small MTU may cause several problems, such as incorrect command interaction or slow [ImageTransportation]
            GlobalScope.launch { mtuConfigChannel.send(20) }
        }
    }

    actual fun requestConnectionPriority(priority: BleConnectionPriority) {
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
            gatt.requestConnectionPriority(
                when (priority) {
                    BleConnectionPriority.Balanced -> CONNECTION_PRIORITY_BALANCED
                    BleConnectionPriority.High -> CONNECTION_PRIORITY_HIGH
                    BleConnectionPriority.LowPower -> CONNECTION_PRIORITY_LOW_POWER
                }
            )
        } else {
            // FIXME Lower priority may cause latency in [NotepadMode.sync]
        }
    }

    actual fun setNotifiable(serviceCharacteristic: Pair<String, String>, inputProperty: BleInputProperty) {
        val descriptor = gatt.getCharacteristic(serviceCharacteristic)?.getDescriptor(DESC__CLIENT_CHAR_CONFIGURATION)
        descriptor?.value = when (inputProperty) {
            BleInputProperty.Disabled -> BluetoothGattDescriptor.DISABLE_NOTIFICATION_VALUE
            BleInputProperty.Notification -> BluetoothGattDescriptor.ENABLE_NOTIFICATION_VALUE
            BleInputProperty.Indication -> BluetoothGattDescriptor.ENABLE_INDICATION_VALUE
        }

        val writeDescriptor =
            descriptor?.let { gatt.setCharacteristicNotification(it.characteristic, true) && gatt.writeDescriptor(it) }
        if (writeDescriptor != true)
            throw Exception("$serviceCharacteristic setNotifiable fail")
    }

    actual fun readValue(serviceCharacteristic: Pair<String, String>) {
        val characteristic = gatt.getCharacteristic(serviceCharacteristic)
        if (characteristic == null || !gatt.readCharacteristic(characteristic))
            throw Exception("$serviceCharacteristic readValue fail")
    }

    actual fun writeValue(serviceCharacteristic: Pair<String, String>, value: ByteArray, outputProperty: BleOutputProperty) {
        val characteristic = gatt.getCharacteristic(serviceCharacteristic)
        characteristic?.value = value
        if (characteristic == null || !gatt.writeCharacteristic(characteristic))
            throw Exception("$serviceCharacteristic writeValue fail")
    }
}

val UUID.uuidString
    get() = this.toString().toUpperCase()

fun BluetoothGatt.getCharacteristic(serviceCharacteristic: Pair<String, String>) =
        getService(UUID.fromString(serviceCharacteristic.first))?.getCharacteristic(UUID.fromString(serviceCharacteristic.second))
