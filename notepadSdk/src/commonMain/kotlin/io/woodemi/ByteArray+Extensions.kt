package io.woodemi

import kotlinx.io.ByteBuffer
import kotlinx.io.ByteOrder
import kotlinx.serialization.internal.getUnsignedInt
import kotlinx.serialization.internal.getUnsignedShort

/**
 * Created by wangkun on 2019/4/17.
 */
fun ByteArray.startWith(prefix: ByteArray): Boolean {
    if (size < prefix.size) return false

    for (i in 0..prefix.size - 1) {
        if (this[i] != prefix[i]) return false
    }
    return true
}

fun List<Byte>.toByteBuffer() = ByteBuffer.allocate(size).also { buffer ->
    forEach { buffer.put(it) }
    buffer.flip()
}

/**
 * fun put(src: ByteArray, offset: Int, cnt: Int): ByteBuffer
 * TODO("Native is not supported yet")
 */
fun ByteArray.toByteBuffer() = toList().toByteBuffer()


val ByteBuffer.uint8
    get() = get().toUByte()

val ByteBuffer.uint16Le
    get() = order(ByteOrder.LITTLE_ENDIAN).getUnsignedShort()

val ByteBuffer.uint32Le
    get() = order(ByteOrder.LITTLE_ENDIAN).getUnsignedInt()

fun ByteBuffer.read(byteArray: ByteArray) {
    for (i in 0 until byteArray.size) {
        byteArray[i] = get()
    }
}

fun Iterable<Byte>.encodeHex() = joinToString("") { it.toUByte().toString(16) }

fun ByteArray.encodeHex() = joinToString("") {
    val b = it.toUByte()
    val prefix = if (b < 16u) "0" else ""
    prefix + b.toString(16)
}

fun String.decodeHex() = windowed(2, 2).map { it.toUByte(16).toByte() }.toByteArray()

fun Short.toByteArray(byteOrder: ByteOrder = ByteOrder.LITTLE_ENDIAN) =
        ByteBuffer.allocate(Short.SIZE_BYTES).order(byteOrder).putShort(this).array()

fun Int.toByteArray(byteOrder: ByteOrder = ByteOrder.LITTLE_ENDIAN) =
        ByteBuffer.allocate(Int.SIZE_BYTES).order(byteOrder).putInt(this).array()
