package io.woodemi

import kotlinx.coroutines.CoroutineDispatcher

/**
 * Created by wangkun on 2019-04-28.
 */
internal expect val ApplicationDispatcher: CoroutineDispatcher

internal expect suspend fun dispatchAfter(timeMillis: Long, block: () -> Unit)