package io.woodemi

/**
 * Created by wangkun on 2019-05-09.
 */
expect object Logger {
    var isEnabled: Boolean

    /**
     * Verbose
     */
    fun v(tag: String, message: String)

    /**
     * Debug
     */
    fun d(tag: String, message: String)

    /**
     * Info
     */
    fun i(tag: String, message: String)

    /**
     * Warning
     */
    fun w(tag: String, message: String)

    /**
     * Error
     */
    fun e(tag: String, message: String)

    /**
     * Severe
     */
    fun s(tag: String, message: String)
}