package io.woodemi

/**
 * Created by wangkun on 2019-05-07.
 */
fun crc16(src: ByteArray): Short {
    // CCITT
    val POLYNOMIAL = 0x1021
    // XMODEM
    val INIT_VALUE = 0x0000
    var crc = INIT_VALUE
    src.forEach {
        crc = crc xor (it.toInt() shl 8)
        for (i in 0 until 8) {
            crc = if (crc and 0x8000 != 0) (crc shl 1) xor POLYNOMIAL else crc shl 1
        }
    }
    return crc.toShort()
}

internal expect fun readUTF8Text(path: String): String

internal data class TagValue(val tag: Byte, val value: ByteArray) {
    companion object {
        fun fromByteArray(bytes: ByteArray): TagValue {
            val buffer = bytes.toByteBuffer()
            val tag = buffer.uint8.toByte()
            val length = buffer.uint8.toInt()
            val value = ByteArray(length).apply { buffer.read(this) }
            return TagValue(tag, value)
        }
    }

    val bytes = byteArrayOf(tag) + value.size.toByte() + value
}

//internal data class LongTagValue(val tag: Short, val value: ByteArray) {
//    companion object {
//        val HEADER_LENGTH = 6
//
//        fun fromByteArray(bytes: ByteArray): LongTagValue {
//            val buffer = bytes.toByteBuffer()
//            val tag = buffer.uint16Le.toShort()
//            val length = buffer.uint32Le.toInt()
//            val value = ByteArray(length).apply { buffer.read(this) }
//            return LongTagValue(tag, value)
//        }
//    }
//
//    val bytes = tag.toByteArray() + value.size.toByteArray() + value
//}

internal data class LongTagValue(val tag: Short, val value: ByteArray) {
    companion object {
        val HEADER_LENGTH = 6

        fun scan(bytes: ByteArray): Map<Int, LongTagValue> {
            val buffer = bytes.toByteBuffer()
            val map = mutableMapOf<Int, LongTagValue>()
            var index = 0
            while (index < bytes.size) {
                val tag = buffer.uint16Le.also { index += 2 }
                val length = buffer.uint32Le.also { index += 4 }
                val value = ByteArray(length.toInt())
                // TODO popFirst instead of ByteBuffer
                buffer.read(value).also { index += value.size }
                map[tag] = LongTagValue(tag.toShort(), value)
            }
            return map
        }
    }

    val bytes = tag.toByteArray() + value.size.toByteArray() + value
}