package io.woodemi.notepad

/**
 * Created by wangkun on 2018/5/9.
 */
enum class DisconnectionCause {
    Unknown, Unauthorized, Unconfirmed
}