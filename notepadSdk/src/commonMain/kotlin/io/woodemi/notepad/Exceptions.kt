package io.woodemi.notepad

sealed class AccessException(message: String) : Exception(message) {
    object Denied : AccessException("Notepad claimed by other user")
    object Unconfirmed : AccessException("User doesn't confirm before timeout")
}