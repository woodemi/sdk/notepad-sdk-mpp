package io.woodemi.notepad

import io.woodemi.notepad.pendo.PendoClient
import io.woodemi.notepad.woodemi.WoodemiClient
import io.woodemi.startWith
import io.woodemi.toByteBuffer
import io.woodemi.uint16Le

/**
 * Created by wangkun on 2019/4/17.
 */
internal object NotepadHelper {
    fun support(manufacturerData: ByteArray?) = when {
        manufacturerData?.startWith(WoodemiClient.A1_UGEE_CN.PREFIX) == true -> true
        manufacturerData?.startWith(WoodemiClient.A1_UGEE_GLOBAL.PREFIX) == true -> true
        manufacturerData?.startWith(WoodemiClient.A1Pr0_EMRIGHT_CN.PREFIX) == true -> true
        manufacturerData?.startWith(PendoClient.PENDO_PDA5.PREFIX) == true -> true
        else -> false
    }

    fun create(manufacturerData: ByteArray?, authToken: ByteArray?): NotepadClient = when {
        manufacturerData?.startWith(WoodemiClient.A1_UGEE_CN.PREFIX) == true -> WoodemiClient(WoodemiClient.A1_UGEE_CN, authToken)
        manufacturerData?.startWith(WoodemiClient.A1_UGEE_GLOBAL.PREFIX) == true -> WoodemiClient(WoodemiClient.A1_UGEE_GLOBAL, authToken)
        manufacturerData?.startWith(WoodemiClient.A1Pr0_EMRIGHT_CN.PREFIX) == true -> WoodemiClient(WoodemiClient.A1Pr0_EMRIGHT_CN, authToken)
        manufacturerData?.startWith(PendoClient.PENDO_PDA5.PREFIX) == true -> PendoClient()
        else -> throw IllegalArgumentException("Unsupported BLE device")
    }
}

expect class NotepadScanResult {
    val name: String?

    val deviceId: String

    val manufacturerData: ByteArray?

    val rssi: Int
}

enum class NotepadMode {
    Sync,
    Common
}

data class NotePenPointer(val x: Int, val y: Int, val t: Long, val p: Int) {
    companion object {
        fun create(bytes: ByteArray): Array<NotePenPointer> {
            val value = bytes.toByteBuffer()
            return Array(bytes.size / 6) {
                NotePenPointer(value.uint16Le, value.uint16Le, -1, value.uint16Le)
            }
        }
    }
}

data class BatteryInfo(val percent: Int, val charging: Boolean)

data class Version(val major: Int, val minor: Int? = null, val patch: Int? = null) {
    val bytes: ByteArray
        get() {
            var data = byteArrayOf(major.toByte())
            if (minor != null) data = data.plus(minor.toByte())
            if (patch != null) data = data.plus(patch.toByte())
            return data
        }
}

data class VersionInfo(val hardware: Version, val software: Version)

data class MemoSummary(
    val memoCount: Int,
    val totalCapacity: Long,
    val freeCapacity: Long,
    val usedCapacity: Long
)

data class MemoInfo(
    val sizeInByte: Long,
    val createdAt: Long,
    val partIndex: Int,
    // Rest part count in current transportation
    val restCount: Int
)

data class MemoData(val memoInfo: MemoInfo, val pointers: List<NotePenPointer>)

sealed class NotepadMessage {
    data class KeyEvent(val type: KeyEventType, val code: KeyEventCode) : NotepadMessage()

    data class ChargingStatusChange(val charging: Boolean) : NotepadMessage()
}

enum class KeyEventCode {
    Main
}

enum class KeyEventType {
    KeyDown, KeyUp
}