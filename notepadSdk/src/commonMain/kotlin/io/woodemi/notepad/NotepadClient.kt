package io.woodemi.notepad

import io.woodemi.ApplicationDispatcher
import io.woodemi.Failure
import kotlinx.coroutines.GlobalScope
import kotlinx.coroutines.flow.collect
import kotlinx.coroutines.launch

val GSS_SUFFIX = "0000-1000-8000-00805F9B34FB"
val CODE__SERV_BATTERY = "180F"
val CODE__CHAR_BATTERY_LEVEL = "2A19"

val SERV__BATTERY = "0000$CODE__SERV_BATTERY-$GSS_SUFFIX"
val CHAR__BATTERY_LEVEL = "0000$CODE__CHAR_BATTERY_LEVEL-$GSS_SUFFIX"

class ClientModel(val PREFIX: ByteArray, val originX: Int, val originY: Int, val width: Int, val height: Int, val pressure: Int, val sizeScale: Double, val pressureScale: Double) {

}

abstract class NotepadClient(val client: ClientModel, val authToken: ByteArray?) {

    internal abstract val commandRequestCharacteristic: Pair<String, String>

    internal abstract val commandResponseCharacteristic: Pair<String, String>

    internal abstract val syncInputCharacteristic: Pair<String, String>

    internal abstract val fileInputControlRequestCharacteristic: Pair<String, String>

    internal abstract val fileInputControlResponseCharacteristic: Pair<String, String>

    internal abstract val fileInputCharacteristic: Pair<String, String>

    internal abstract val fileOutputControlRequestCharacteristic: Pair<String, String>

    internal abstract val fileOutputControlResponseCharacteristic: Pair<String, String>

    internal abstract val fileOutputCharacteristic: Pair<String, String>

    internal abstract val inputIndicationCharacteristics: List<Pair<String, String>>

    internal abstract val inputNotificationCharacteristics: List<Pair<String, String>>

    // TODO Remove recursively [NotepadType] & [NotepadClient] reference
    internal var notepadType: NotepadType? = null

    internal open suspend fun completeConnection(awaitConfirm: (Boolean) -> Unit) {
        // TODO Cancel job if disconnected
        GlobalScope.launch(ApplicationDispatcher) {
            notepadType!!.receiveSyncInput().collect {
                callback?.handlePointer(parseSyncData(it).toList())
            }
        }
    }

    abstract fun claimAuth(complete: () -> Unit, error: (Failure) -> Unit)

    abstract fun disclaimAuth(complete: () -> Unit, error: (Failure) -> Unit)

    abstract fun getDeviceName(success: (String) -> Unit, error: (Failure) -> Unit)

    abstract fun setDeviceName(to: String, complete: () -> Unit, error: (Failure) -> Unit)

    abstract fun getBatteryInfo(success: (BatteryInfo) -> Unit, error: (Failure) -> Unit)

    abstract fun getDeviceDate(success: (Long) -> Unit, error: (Failure) -> Unit)

    abstract fun setDeviceDate(to: Long, complete: () -> Unit, error: (Failure) -> Unit)

    abstract fun getAutoLockTime(success: (Int) -> Unit, error: (Failure) -> Unit)

    abstract fun setAutoLockTime(to: Int, complete: () -> Unit, error: (Failure) -> Unit)

    abstract fun getVersionInfo(success: (VersionInfo) -> Unit, error: (Failure) -> Unit)

    interface Callback {
        fun handlePointer(list: List<NotePenPointer>)
        fun handleEvent(message: NotepadMessage)
    }

    var callback: Callback? = null

    abstract fun setMode(to: NotepadMode, complete: () -> Unit, error: (Failure) -> Unit)

    protected abstract fun parseSyncData(bytes: ByteArray): Array<NotePenPointer>

    abstract fun getMemoSummary(success: (MemoSummary) -> Unit, error: (Failure) -> Unit)

    abstract fun getMemoInfo(success: (MemoInfo) -> Unit, error: (Failure) -> Unit)

    abstract fun importMemo(progress: (Int) -> Unit, success: (MemoData) -> Unit, error: (Failure) -> Unit)

    abstract fun deleteMemo(complete: () -> Unit, error: (Failure) -> Unit)

    abstract fun upgrade(with: String, version: Version, progress: (Int) -> Unit, complete: () -> Unit, error: (Failure) -> Unit)
}

internal val NotepadClient.inputServices
    get() = (inputIndicationCharacteristics + inputNotificationCharacteristics).map { it.first }.distinct()