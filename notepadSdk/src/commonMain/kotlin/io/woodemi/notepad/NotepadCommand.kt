package io.woodemi.notepad

const val COMMAND_TIMEOUT_MS: Long = 2000

abstract class NotepadCommand<Response>(
        val request: ByteArray,
        val intercept: (ByteArray) -> Boolean,
        val handle: (ByteArray) -> Response
)