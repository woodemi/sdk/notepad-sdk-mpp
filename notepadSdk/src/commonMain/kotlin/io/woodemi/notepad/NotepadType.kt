package io.woodemi.notepad

import io.woodemi.ApplicationDispatcher
import io.woodemi.Logger
import kotlinx.coroutines.GlobalScope
import kotlinx.coroutines.async
import kotlinx.coroutines.channels.BroadcastChannel
import kotlinx.coroutines.channels.Channel
import kotlinx.coroutines.channels.SendChannel
import kotlinx.coroutines.flow.*
import kotlinx.coroutines.launch
import kotlin.properties.Delegates

/**
 * Created by wangkun on 2019-04-26.
 */
const val GATT_HEADER_LENGTH = 3

enum class BleInputProperty {
    Disabled, Notification, Indication
}

enum class BleOutputProperty {
    WithResponse, WithoutResponse
}

enum class BleConnectionPriority {
    Balanced, High, LowPower
}

internal expect class BleType {
    val mtuConfigChannel: Channel<Int>

    fun requestMtu(expectedMtu: Int)

    fun setNotifiable(serviceCharacteristic: Pair<String, String>, inputProperty: BleInputProperty)

    fun requestConnectionPriority(priority: BleConnectionPriority)

    fun readValue(serviceCharacteristic: Pair<String, String>)

    fun writeValue(serviceCharacteristic: Pair<String, String>, value: ByteArray, outputProperty: BleOutputProperty)
}

internal class NotepadType(
    internal val notepadClient: NotepadClient,
    private val bleType: BleType
) {
    private val TAG = toString()

    init {
        notepadClient.notepadType = this
    }

    private val _characteristicConfigChannel = Channel<Pair<String, String>>()
    val characteristicConfigChannel: SendChannel<Pair<String, String>> = _characteristicConfigChannel

    suspend fun configCharacteristics() {
        notepadClient.inputIndicationCharacteristics.forEach {
            configInputCharacteristic(it, BleInputProperty.Indication)
        }
        notepadClient.inputNotificationCharacteristics.forEach {
            configInputCharacteristic(it, BleInputProperty.Notification)
        }
    }

    private suspend fun configInputCharacteristic(
        serviceCharacteristic: Pair<String, String>,
        inputProperty: BleInputProperty
    ) {
        Logger.d(TAG, "configInputCharacteristic $serviceCharacteristic, $inputProperty")
        bleType.setNotifiable(serviceCharacteristic, inputProperty)

        // TODO Receive with timeout
        if (_characteristicConfigChannel.receive() != serviceCharacteristic)
            throw Exception("$serviceCharacteristic not confirmed")
    }

    var mtu by Delegates.notNull<Int>()

    val mtuConfigChannel: SendChannel<Int> = bleType.mtuConfigChannel

    suspend fun configMtu(expected: Int) {
        bleType.requestMtu(expected)
        mtu = bleType.mtuConfigChannel.receive()
    }

    fun requestConnectionPriority(priority: BleConnectionPriority) {
        bleType.requestConnectionPriority(priority)
    }

    private val _outputValueChannel = Channel<Pair<String, ByteArray>>()
    val outputValueChannel: SendChannel<Pair<String, ByteArray>> = _outputValueChannel

    suspend fun sendValue(serviceCharacteristic: Pair<String, String>, value: ByteArray, outputProperty: BleOutputProperty = BleOutputProperty.WithResponse) {
        bleType.writeValue(serviceCharacteristic, value, outputProperty)
        if (outputProperty == BleOutputProperty.WithoutResponse) return

        // TODO Receive with timeout
        val (writeCharacteristic, writeValue) = _outputValueChannel.receive()
        Logger.d(TAG, "onValueSend: $writeCharacteristic ${writeValue.contentToString()}")

        // FIXME writeValue unable to confirm on iOS
        if (writeCharacteristic != serviceCharacteristic.second)
            throw Exception("$serviceCharacteristic sendValue not confirmed")
    }

    fun sendRequestAsync(
            messageHead: String,
            serviceCharacteristic: Pair<String, String>,
            request: ByteArray
    ) = GlobalScope.launch(ApplicationDispatcher) {
        sendValue(serviceCharacteristic, request)
        Logger.d(TAG, "on${messageHead}Send: ${request.contentToString()}")
    }

    private val _inputValueChannel = BroadcastChannel<Pair<String, ByteArray>>(10)
    val inputValueChannel: SendChannel<Pair<String, ByteArray>> = _inputValueChannel

    fun receiveValue(serviceCharacteristic: Pair<String, String>) =
            _inputValueChannel.asFlow().filter {
                it.first == serviceCharacteristic.second || "0000${it.first}-$GSS_SUFFIX" == serviceCharacteristic.second
            }.map { it.second }

    fun receiveResponseAsync(
        messageHead: String,
        serviceCharacteristic: Pair<String, String>,
        predicate: suspend (ByteArray) -> Boolean
    ) = GlobalScope.async(ApplicationDispatcher) {
        receiveValue(serviceCharacteristic).filter(predicate).take(1).single().also {
            Logger.d(TAG, "on${messageHead}Receive: ${it.contentToString()}")
        }
    }

    suspend fun <Response> fetchProperty(serviceCharacteristic: Pair<String, String>, handle: (ByteArray) -> Response): Response {
        val response = receiveResponseAsync("Property", serviceCharacteristic) { true }
        bleType.readValue(serviceCharacteristic)
        return handle(response.await())
    }

    // FIXME Somehow [withTimeout] won't work on iOS
    suspend fun <Response> executeCommand(command: NotepadCommand<Response>): Response {
        val request = sendRequestAsync("Command", notepadClient.commandRequestCharacteristic, command.request)

        // TODO Check flow cancellation
        val response = receiveResponseAsync(
            "Command",
            notepadClient.commandResponseCharacteristic
        ) { command.intercept(it) }

        request.join()
        return command.handle(response.await())
    }

    fun receiveSyncInput() = receiveValue(notepadClient.syncInputCharacteristic).onEach {
        Logger.d(TAG, "onSyncInputReceive: ${it.contentToString()}")
    }

    // FIXME Somehow [withTimeout] won't work on iOS
    suspend fun <Response> executeFileInputControl(command: NotepadCommand<Response>): Response {
        val request = sendRequestAsync("FileInputControl", notepadClient.fileInputControlRequestCharacteristic, command.request)

        // TODO Check flow cancellation
        val response = receiveResponseAsync(
            "FileInputControl",
            notepadClient.fileInputControlResponseCharacteristic
        ) { command.intercept(it) }

        request.join()
        return command.handle(response.await())
    }

    fun receiveFileInput() = receiveValue(notepadClient.fileInputCharacteristic).onEach {
        Logger.d(TAG, "onFileInputReceive: ${it.contentToString()}")
    }
}
