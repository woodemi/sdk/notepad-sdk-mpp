package io.woodemi.notepad.pendo

import io.woodemi.ApplicationDispatcher
import io.woodemi.Failure
import io.woodemi.TagValue
import io.woodemi.notepad.*
import kotlinx.coroutines.GlobalScope
import kotlinx.coroutines.launch

/**
 * Created by wangkun on 2019-07-01.
 */
private const val SUFFIX = "2877-41C3-B46E-CF057C562023"

private const val SERV__COMMAND = "1B7E8271-$SUFFIX"
private const val CHAR__COMMAND_REQUEST = "1B7E8272-$SUFFIX"
private const val CHAR__COMMAND_RESPONSE = "1B7E8273-$SUFFIX"

private const val SERV__SYNC = "1B7E8261-$SUFFIX"
private const val CHAR__SYNC_INPUT = "1B7E8262-$SUFFIX"

private const val SERV__FILE_IO = "1B7E8281-$SUFFIX"
private const val CHAR__FILE_OUTPUT = "1B7E8282-$SUFFIX"
private const val CHAR__FILE_INPUT = "1B7E8283-$SUFFIX"


internal class PendoClient : NotepadClient(PendoClient.PENDO_PDA5, null) {
    companion object {
        val PENDO_PDA5 = ClientModel(byteArrayOf(0x50, 0x44, 0x41, 0x35), 0, 0, 14800, 21000, 512, 1.0, 1.0)
    }

    override val commandRequestCharacteristic = SERV__COMMAND to CHAR__COMMAND_REQUEST

    override val commandResponseCharacteristic = SERV__COMMAND to CHAR__COMMAND_RESPONSE

    override val syncInputCharacteristic = SERV__SYNC to CHAR__SYNC_INPUT

    override val fileInputControlRequestCharacteristic = "" to "" // FIXME

    override val fileInputControlResponseCharacteristic = "" to "" // FIXME

    override val fileInputCharacteristic = SERV__FILE_IO to CHAR__FILE_INPUT

    override val fileOutputControlRequestCharacteristic = "" to "" // FIXME

    override val fileOutputControlResponseCharacteristic = "" to "" // FIXME

    override val fileOutputCharacteristic = SERV__FILE_IO to CHAR__FILE_OUTPUT

    override val inputIndicationCharacteristics = emptyList<Pair<String, String>>()

    override val inputNotificationCharacteristics = listOf(
            commandResponseCharacteristic,
            syncInputCharacteristic,
            fileInputCharacteristic
    )

    override fun claimAuth(complete: () -> Unit, error: (Failure) -> Unit) {
        GlobalScope.launch(ApplicationDispatcher) {
            error(Failure(Exception("not implemented")))
        }
    }

    override fun disclaimAuth(complete: () -> Unit, error: (Failure) -> Unit) {
        GlobalScope.launch(ApplicationDispatcher) {
            error(Failure(Exception("not implemented")))
        }
    }

    override fun getDeviceName(success: (String) -> Unit, error: (Failure) -> Unit) {
        GlobalScope.launch(ApplicationDispatcher) {
            error(Failure(Exception("not implemented")))
        }
    }

    override fun setDeviceName(to: String, complete: () -> Unit, error: (Failure) -> Unit) {
        GlobalScope.launch(ApplicationDispatcher) {
            error(Failure(Exception("not implemented")))
        }
    }

    override fun getBatteryInfo(success: (BatteryInfo) -> Unit, error: (Failure) -> Unit) {
        GlobalScope.launch(ApplicationDispatcher) {
            error(Failure(Exception("not implemented")))
        }
    }

    override fun getDeviceDate(success: (Long) -> Unit, error: (Failure) -> Unit) {
        GlobalScope.launch(ApplicationDispatcher) {
            error(Failure(Exception("not implemented")))
        }
    }

    override fun setDeviceDate(to: Long, complete: () -> Unit, error: (Failure) -> Unit) {
        GlobalScope.launch(ApplicationDispatcher) {
            error(Failure(Exception("not implemented")))
        }
    }

    override fun getAutoLockTime(success: (Int) -> Unit, error: (Failure) -> Unit) {
        GlobalScope.launch(ApplicationDispatcher) {
            error(Failure(Exception("not implemented")))
        }
    }

    override fun setAutoLockTime(to: Int, complete: () -> Unit, error: (Failure) -> Unit) {
        GlobalScope.launch(ApplicationDispatcher) {
            error(Failure(Exception("not implemented")))
        }
    }

    override fun getVersionInfo(success: (VersionInfo) -> Unit, error: (Failure) -> Unit) {
        GlobalScope.launch(ApplicationDispatcher) {
            error(Failure(Exception("not implemented")))
        }
    }

    override fun setMode(to: NotepadMode, complete: () -> Unit, error: (Failure) -> Unit) {
        val command = when (to) {
            NotepadMode.Sync -> PendoCommand(0x021, byteArrayOf(0), handle = PendoCommand.defaultHandle)
            NotepadMode.Common -> PendoCommand(0x021, byteArrayOf(1), handle = PendoCommand.defaultHandle)
        }
        GlobalScope.launch(ApplicationDispatcher) {
            try {
                notepadType!!.executeCommand(command)
                complete()
            } catch (e: Exception) {
                error(Failure(e))
            }
        }
    }

    override fun parseSyncData(bytes: ByteArray): Array<NotePenPointer> {
        val (tag, value) = TagValue.fromByteArray(bytes)
        // Characteristic value format type uint8
        return if (tag == 0x11.toByte()) NotePenPointer.create(value) else emptyArray()
    }

    override fun getMemoSummary(success: (MemoSummary) -> Unit, error: (Failure) -> Unit) {
        TODO("not implemented") //To change body of created functions use File | Settings | File Templates.
    }

    override fun getMemoInfo(success: (MemoInfo) -> Unit, error: (Failure) -> Unit) {
        TODO("not implemented") //To change body of created functions use File | Settings | File Templates.
    }

    override fun importMemo(progress: (Int) -> Unit, success: (MemoData) -> Unit, error: (Failure) -> Unit) {
        TODO("not implemented") //To change body of created functions use File | Settings | File Templates.
    }

    override fun deleteMemo(complete: () -> Unit, error: (Failure) -> Unit) {
        TODO("not implemented") //To change body of created functions use File | Settings | File Templates.
    }

    override fun upgrade(with: String, version: Version, progress: (Int) -> Unit, complete: () -> Unit, error: (Failure) -> Unit) {
        TODO("not implemented") //To change body of created functions use File | Settings | File Templates.
    }
}