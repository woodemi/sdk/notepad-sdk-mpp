package io.woodemi.notepad.pendo

import io.woodemi.TagValue
import io.woodemi.notepad.NotepadCommand

/**
 * Created by wangkun on 2019-07-01.
 */

private const val TAG__RESP_COMMON: Byte = 0x23
private const val VALUE__RESP_SUCCESS: Byte = 0

internal class PendoCommand<Response>(
        request: ByteArray,
        intercept: (ByteArray) -> Boolean = { it.first() == TAG__RESP_COMMON && it[1] == request.first() },
        handle: (ByteArray) -> Response
) : NotepadCommand<Response>(request, intercept, handle) {
    companion object {
        val defaultHandle: (ByteArray) -> Unit = {
            val (tag, value) = TagValue.fromByteArray(it)
            if (!value.contentEquals(byteArrayOf(VALUE__RESP_SUCCESS)))
                throw Exception("PENDO_COMMAND fail: response ${it.contentToString()}")
        }
    }

    constructor(
            requestTag: Byte,
            requestValue: ByteArray,
            responseTag: Byte = TAG__RESP_COMMON,
            handle: (ByteArray) -> Response
    ) : this(
            TagValue(requestTag, requestValue).bytes,
            { TagValue.fromByteArray(it).tag == responseTag },
            handle
    )
}