package io.woodemi.notepad.woodemi

import io.woodemi.decodeHex
import io.woodemi.readUTF8Text
import kotlin.experimental.inv
import kotlin.properties.Delegates

internal fun parseUpgradeFile(path: String): ByteArray {
    val lines = readUTF8Text(path).split("\r\n")
    val records = lines
            .filter { it.isNotEmpty() }
            .map { FileRecord(it) }
            .filter { FileRecord.RECORD_TYPE_DATAs.contains(it.tag) }

    val start = records.first().address
    val end = records.last().address + records.last().data.size
    return ByteArray((end - start).toInt()) { 0xFF.toByte() }.apply {
        records.forEach {
            it.data.copyInto(this, (it.address - start).toInt())
        }
    }
}


/**
 * In Motorola S-records(S19) file format
 * A single line is a [FileRecord]
 */
internal class FileRecord private constructor() {
    var tag: String by Delegates.notNull()
    private var length: Int by Delegates.notNull()
    private var value: ByteArray by Delegates.notNull()

    var address: Long by Delegates.notNull()
    var data: ByteArray by Delegates.notNull()

    companion object {
        val RECORD_TYPE_HEADER = "S0" // 16 bit address
        val RECORD_TYPE_DATA_1 = "S1" // 16 bit address
        val RECORD_TYPE_DATA_2 = "S2" // 24 bit address
        val RECORD_TYPE_DATA_3 = "S3" // 32 bit address
        val RECORD_TYPE_TERMINATION = "S8" //
        val RECORD_TYPE_DATAs = arrayOf(RECORD_TYPE_DATA_1, RECORD_TYPE_DATA_2, RECORD_TYPE_DATA_3)
    }

    /**
     * +----------+----------+----------+----------+-----------+
     * |   tag    |  length  |            value                |
     * +----------+----------+----------+----------+-----------+
     * |          |          |         length  bytes           |
     * |          |          +----------+----------+-----------+
     * | 2 chars  |  1 byte  |  address |  data    |  checksum |
     * |          |          +----------+----------+-----------+
     * |          |          |  X bytes |  ...     |  1 byte   |
     * +----------+----------+----------+----------+-----------+
     */
    constructor(line: String) : this() {
        tag = line.substring(0..1)
        length = line.substring(2..3).toInt(16)
        value = line.substring(4).decodeHex()
        if (length != value.size) throw AssertionError("Invalid value size")

        val checksum = value.last()
        val arrayToCheck = byteArrayOf(length.toByte()) + value.dropLast(1 /*checksum*/)
        val sum = arrayToCheck.reduce { acc, byte -> (acc + byte).toByte() }
        if (sum.inv() != checksum) throw AssertionError("Invalid checksum")

        val addressLength = when (tag) {
            RECORD_TYPE_HEADER, RECORD_TYPE_DATA_1, RECORD_TYPE_TERMINATION -> 2
            RECORD_TYPE_DATA_2 -> 3
            RECORD_TYPE_DATA_3 -> 4
            else -> 2
        }
        address = line.substring(4, 4 + addressLength * 2).toLong(16)
        data = value.sliceArray(addressLength until value.size)
    }
}