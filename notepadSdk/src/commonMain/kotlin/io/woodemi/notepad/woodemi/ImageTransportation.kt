package io.woodemi.notepad.woodemi

import io.woodemi.*
import io.woodemi.notepad.Version
import kotlin.properties.Delegates

internal class ImageTransportation() {
    /**
     * +--------------------------------------------------------------------------------------------------------------------------------+
     * |                                                             header                                                             |
     * +----------+----------------+---------------+---------------+------------+----------+---------------+---------------+------------+
     * | fileId   |  headerVersion |  headerLength |  fieldControl |  companyId |  imageId |  imageVersion |  headerString |  totalSize |
     * |          |                |               |               |            |          |               |               |            |
     * | 4 bytes  |  2 bytes       |  2 bytes      |  2 bytes      |  2 bytes   |  2 bytes |  8 bytes      |  32 bytes     |  4 bytes   |
     * +----------+----------------+---------------+---------------+------------+----------+---------------+---------------+------------+
     */
    private var header = object {
        var fileId = ByteArray(4) { 0 }
        var headerVersion = ByteArray(2) { 0 }
        var fieldControl = ByteArray(2) { 0 }
        var companyId = ByteArray(2) { 0 }

        var imageId = ByteArray(2) { 0 }
        var imageVersion = ByteArray(8) { 0 }
        var headerString = ByteArray(32) { 0 }
        var totalSize: Int by Delegates.notNull()

        fun fromByteArray(bytes: ByteArray) {
            val buffer = bytes.toByteBuffer()
            buffer.read(fileId)
            buffer.read(headerVersion)
            val headerLength = buffer.uint16Le
            // TODO popFirst instead of ByteBuffer
            if (headerLength != HEADER_LENGTH) throw AssertionError("Invalid headerLength")

            buffer.read(fieldControl)
            buffer.read(companyId)
            buffer.read(imageId)
            buffer.read(imageVersion)
            buffer.read(headerString)
            totalSize = buffer.uint32Le.toInt()
        }

        fun toByteArray(): ByteArray {
            totalSize = getTotalSize()
            val headerBytes = (fileId + headerVersion + HEADER_LENGTH.toShort().toByteArray()
                    + fieldControl + companyId + imageId + imageVersion + headerString + totalSize.toByteArray())
            if (headerBytes.size != HEADER_LENGTH) throw AssertionError("Invalid headerLength")
            return headerBytes
        }

    }

    companion object {
        private val HEADER_LENGTH = 58
        val EMPTY_LENGTH = HEADER_LENGTH + 6 /*empty imageTagValue*/ + 8 /*crcTagValue*/
        val TAG_IMAGE = 0x0000
        val TAG_BITMAP = 0xF000
        val TAG_CRC = 0xF100

        @ExperimentalStdlibApi
        fun build(imageData: ByteArray, version: Version): ImageTransportation {
            val transportation = ImageTransportation()
            transportation.header.fileId = "1EF11E0B".decodeHex()
            transportation.header.headerVersion = "0001".decodeHex()
            transportation.header.fieldControl = "0000".decodeHex()
            transportation.header.companyId = "FF01".decodeHex()
            transportation.header.imageId = "0100".decodeHex()
            val buildVersion = version.bytes.reversedArray()
            transportation.header.imageVersion = buildVersion + "4111111101".decodeHex()
            transportation.header.headerString = "FSL BLE OTAP Demo Image File".encodeToByteArray().copyOf(32)

            transportation.imageData = imageData
            transportation.bitmapData = ByteArray(32) { 0xFF.toByte() }
            return transportation
        }
    }

    val imageId get() = header.imageId
    val imageVersion get() = header.imageVersion
    var imageData by Delegates.notNull<ByteArray>()
    private var bitmapData: ByteArray? = null
    private var crc: ByteArray = ByteArray(2) { 0 }

    fun getTotalSize() = (HEADER_LENGTH
            + LongTagValue.HEADER_LENGTH + imageData.size
            + LongTagValue.HEADER_LENGTH + bitmapData!!.size
            + LongTagValue.HEADER_LENGTH + crc.size)

    constructor(bytes: ByteArray) : this() {
        header.fromByteArray(bytes.sliceArray(0 until HEADER_LENGTH))
        val pendingData = bytes.drop(HEADER_LENGTH).toByteArray()
        if (header.totalSize != HEADER_LENGTH + pendingData.size) throw AssertionError("Invalid totalSize")

//        val map = scanLongTagValue(pendingData)
        val map = LongTagValue.scan(pendingData)
        imageData = map[TAG_IMAGE]!!.value
        bitmapData = map[TAG_BITMAP]?.value
        crc = map[TAG_CRC]!!.value

        if (!crc16(imageData).toByteArray().contentEquals(crc)) throw Exception("Data CRC fail")
    }

    fun toByteArray(): ByteArray {
        val byteArray = header.toByteArray() + LongTagValue(TAG_IMAGE.toShort(), imageData).bytes + LongTagValue(TAG_BITMAP.toShort(), bitmapData!!).bytes
        val totalData = byteArray + LongTagValue(TAG_CRC.toShort(), crc16(byteArray).toByteArray()).bytes
        if (totalData.size != header.totalSize) throw AssertionError("Invalid totalSize")
        return totalData
    }
}

//private fun scanLongTagValue(bytes: ByteArray): Map<Int, LongTagValue> {
//    val map = mutableMapOf<Int, LongTagValue>()
//    var rest = bytes
//    while (rest.isNotEmpty()) {
//        val tagValue = LongTagValue.fromByteArray(rest)
//        map[tagValue.tag.toInt()] = tagValue
//        rest = rest.drop(tagValue.bytes.size).toByteArray()
//    }
//    return map
//}

data class LongTagValue(val tag: Short, val value: ByteArray) {
    companion object {
        val HEADER_LENGTH = 6

        fun scan(bytes: ByteArray): Map<Int, LongTagValue> {
            val buffer = bytes.toByteBuffer()
            val map = mutableMapOf<Int, LongTagValue>()
            var index = 0
            while (index < bytes.size) {
                val tag = buffer.uint16Le.also { index += 2 }
                val length = buffer.uint32Le.also { index += 4 }
                val value = ByteArray(length.toInt())
                // TODO popFirst instead of ByteBuffer
                buffer.read(value).also { index += value.size }
                map[tag] = LongTagValue(tag.toShort(), value)
            }
            return map
        }
    }

    val bytes = tag.toByteArray() + value.size.toByteArray() + value
}