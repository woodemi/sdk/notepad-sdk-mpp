package io.woodemi.notepad.woodemi

import io.woodemi.*
import io.woodemi.notepad.*
import kotlinx.coroutines.GlobalScope
import kotlinx.coroutines.flow.*
import kotlinx.coroutines.launch
import kotlinx.serialization.stringFromUtf8Bytes
import kotlinx.serialization.toUtf8Bytes
import kotlin.math.ceil
import kotlin.math.min

/**
 * Created by wangkun on 2019/4/17.
 */
private const val SUFFIX = "BA5E-F4EE-5CA1-EB1E5E4B1CE0"

private const val SERV__COMMAND = "57444D01-$SUFFIX"
private const val CHAR__COMMAND_REQUEST = "57444E02-$SUFFIX"
private const val CHAR__COMMAND_RESPONSE = CHAR__COMMAND_REQUEST

private const val SERV__SYNC = "57444D06-$SUFFIX"
private const val CHAR__SYNC_INPUT = "57444D07-$SUFFIX"

private const val SERV__FILE_INPUT = "57444D03-$SUFFIX"
private const val CHAR__FILE_INPUT_CONTROL_REQUEST = "57444D04-$SUFFIX"
private const val CHAR__FILE_INPUT_CONTROL_RESPONSE = CHAR__FILE_INPUT_CONTROL_REQUEST
private const val CHAR__FILE_INPUT = "57444D05-$SUFFIX"

private const val SERV__FILE_OUTPUT = "01FF5550-$SUFFIX"
private const val CHAR__FILE_OUTPUT_CONTROL_REQUEST = "01FF5551-$SUFFIX"
private const val CHAR__FILE_OUTPUT_CONTROL_RESPONSE = CHAR__FILE_OUTPUT_CONTROL_REQUEST
private const val CHAR__FILE_OUTPUT = "01FF5552-$SUFFIX"

private val DEFAULT_AUTH_TOKEN = byteArrayOf(0, 0, 0, 1)

private const val MTU_WUART = 247

private const val SAMPLE_INTERVAL_MS = 5

private const val MAX_NAME_LENGTH = 15

private const val DEVICE_PROCESSING_INTERVAL = 110

internal class WoodemiClient(client: ClientModel, authToken: ByteArray?) : NotepadClient(client, authToken) {
    private val TAG = toString()

    companion object {
        val A1_UGEE_CN = ClientModel(byteArrayOf(0x57, 0x44, 0x4d, 0x41, 0x35), 0, 0, 14800, 21000, 512, 1.0, 1.0)
        val A1_UGEE_GLOBAL = ClientModel(byteArrayOf(0x57, 0x44, 0x4d, 0x41, 0x36), 0, 0, 14800, 21000, 512, 1.0, 1.0)
        val A1Pr0_EMRIGHT_CN = ClientModel(byteArrayOf(0x57, 0x44, 0x4d, 0x41, 0x37), -500, 0, 14800, 21000, 2048, 1.0, 0.25)

        fun parseMemo(bytes: ByteArray, createdAt: Long, client: ClientModel): List<NotePenPointer> {
            val dataArray= bytes.toList().windowed(6, 6)
            var start = createdAt
            return dataArray.fold(listOf()) { pointers, byteList ->
                val value = byteList.toByteBuffer()
                if (byteList[4] == 0xFF.toByte() && byteList[5] == 0xFF.toByte()) {
                    start = value.uint32Le * 1000
                    pointers
                } else {
                    val t = start
                    start += SAMPLE_INTERVAL_MS
                    pointers.plus(NotePenPointer(
                        ((client.originX + value.uint16Le).toDouble() * client.sizeScale).toInt(),
                        ((client.originX + value.uint16Le).toDouble() * client.sizeScale).toInt(),
                            t,
                        (value.uint16Le * client.pressureScale).toInt()))
                }
            }
        }
    }

    override val commandRequestCharacteristic = SERV__COMMAND to CHAR__COMMAND_REQUEST

    override val commandResponseCharacteristic = SERV__COMMAND to CHAR__COMMAND_RESPONSE

    private val messageInputCharacteristic = commandResponseCharacteristic

    override val syncInputCharacteristic = SERV__SYNC to CHAR__SYNC_INPUT

    override val fileInputControlRequestCharacteristic = SERV__FILE_INPUT to CHAR__FILE_INPUT_CONTROL_REQUEST

    override val fileInputControlResponseCharacteristic = SERV__FILE_INPUT to CHAR__FILE_INPUT_CONTROL_RESPONSE

    override val fileInputCharacteristic = SERV__FILE_INPUT to CHAR__FILE_INPUT

    override val fileOutputControlRequestCharacteristic = SERV__FILE_OUTPUT to CHAR__FILE_OUTPUT_CONTROL_REQUEST

    override val fileOutputControlResponseCharacteristic = SERV__FILE_OUTPUT to CHAR__FILE_OUTPUT_CONTROL_RESPONSE

    override val fileOutputCharacteristic = SERV__FILE_OUTPUT to CHAR__FILE_OUTPUT

    override val inputIndicationCharacteristics = listOf(
            commandResponseCharacteristic,
            fileInputControlResponseCharacteristic,
            fileOutputControlResponseCharacteristic
    )

    override val inputNotificationCharacteristics = listOf(
            syncInputCharacteristic,
            fileInputCharacteristic
    )

    override suspend fun completeConnection(awaitConfirm: (Boolean) -> Unit) {
        super.completeConnection(awaitConfirm)
        listenMessageInput()
        notepadType!!.configMtu(MTU_WUART)
        when (checkAccess(authToken ?: DEFAULT_AUTH_TOKEN, 10, awaitConfirm)) {
            AccessResult.Denied -> throw AccessException.Denied
            AccessResult.Confirmed -> {
                // TODO Claim authorization
            }
            AccessResult.Unconfirmed -> throw AccessException.Unconfirmed
            AccessResult.Approved -> {
                // Empty by design
            }
        }
    }

    private fun listenMessageInput() {
        GlobalScope.launch(ApplicationDispatcher) {
            notepadType!!.receiveValue(messageInputCharacteristic)
                    .filter { it.first() == 0x06.toByte() }
                    .onEach { Logger.d(TAG, "onMessageInputReceive: ${it.contentToString()}") }
                    .collect {
                        when (it[1]) {
                            0x00.toByte() -> callback?.handleEvent(
                                    NotepadMessage.KeyEvent(
                                            KeyEventType.KeyUp,
                                            KeyEventCode.Main
                                    )
                            )
                            0x01.toByte() -> callback?.handleEvent(NotepadMessage.ChargingStatusChange(it[2] == 0x01.toByte()))
                        }
                    }
        }
    }

    private enum class AccessResult {
        Denied,      // Device claimed by other user
        Confirmed,   // Access confirmed, indicating device not claimed by anyone
        Unconfirmed, // Access unconfirmed, as user doesn't confirm before timeout
        Approved     // Device claimed by this user
    }

    private suspend fun checkAccess(authToken: ByteArray, seconds: Int, awaitConfirm: (Boolean) -> Unit): AccessResult {
        val command =
            WoodemiCommand(byteArrayOf(0x01, seconds.toByte(), *authToken), { it.first() == 0x02.toByte() }, { it[1] })
        return when (notepadType!!.executeCommand(command)) {
            0x00.toByte() -> AccessResult.Denied
            0x01.toByte() -> {
                awaitConfirm(true)
                val confirm = notepadType!!.receiveResponseAsync(
                    "Confirm",
                    commandResponseCharacteristic
                ) { it.first() == 0x03.toByte() }
                if (confirm.await()[1] == 0x00.toByte()) AccessResult.Confirmed else AccessResult.Unconfirmed
            }
            0x02.toByte() -> AccessResult.Approved
            else -> throw Exception("Unknown error")
        }
    }

    override fun claimAuth(complete: () -> Unit, error: (Failure) -> Unit) {
        val token = authToken ?: DEFAULT_AUTH_TOKEN
        val command = WoodemiCommand(byteArrayOf(0x04, 0x00, *token), handle = WoodemiCommand.defaultHandle)
        GlobalScope.launch(ApplicationDispatcher) {
            try {
                notepadType!!.executeCommand(command)
                complete()
            } catch (e: Exception) {
                error(Failure(e))
            }
        }
    }

    override fun disclaimAuth(complete: () -> Unit, error: (Failure) -> Unit) {
        val token = authToken ?: DEFAULT_AUTH_TOKEN
        val command = WoodemiCommand(byteArrayOf(0x04, 0x01, *token), handle = WoodemiCommand.defaultHandle)
        GlobalScope.launch(ApplicationDispatcher) {
            try {
                notepadType!!.executeCommand(command)
                complete()
            } catch (e: Exception) {
                error(Failure(e))
            }
        }
    }

    override fun getDeviceName(success: (String) -> Unit, error: (Failure) -> Unit) {
        val command = WoodemiCommand(byteArrayOf(0x08, 0x04), { it.first() == 0x0F.toByte() }) {
            stringFromUtf8Bytes(it.drop(1).toByteArray())
        }
        GlobalScope.launch(ApplicationDispatcher) {
            try {
                val name = notepadType!!.executeCommand(command)
                success(name)
            } catch (e: Exception) {
                error(Failure(e))
            }
        }
    }

    override fun setDeviceName(to: String, complete: () -> Unit, error: (Failure) -> Unit) {
        val source = to.toUtf8Bytes()
        val destination = ByteArray(MAX_NAME_LENGTH) { 0x00.toByte() }
        val argument = source.copyInto(destination, endIndex = min(source.size, destination.size))
        val command = WoodemiCommand(byteArrayOf(0x0B, *argument), handle = WoodemiCommand.defaultHandle)
        GlobalScope.launch(ApplicationDispatcher) {
            try {
                notepadType!!.executeCommand(command)
                complete()
            } catch (e: Exception) {
                error(Failure(e))
            }
        }
    }

    override fun getBatteryInfo(success: (BatteryInfo) -> Unit, error: (Failure) -> Unit) {
        val command = WoodemiCommand(byteArrayOf(0x08, 0x03), {
            it[0] == 0x06.toByte() && it[1] == 0x01.toByte()
        }) {
            it[2] == 0x01.toByte()
        }
        GlobalScope.launch(ApplicationDispatcher) {
            try {
                val percent = notepadType!!.fetchProperty(SERV__BATTERY to CHAR__BATTERY_LEVEL) {
                    it.toByteBuffer().uint8.toInt()
                }
                val charging = notepadType!!.executeCommand(command)
                success(BatteryInfo(percent, charging))
            } catch (e: Exception) {
                error(Failure(e))
            }
        }
    }

    override fun getDeviceDate(success: (Long) -> Unit, error: (Failure) -> Unit) {
        val command = WoodemiCommand(byteArrayOf(0x08, 0x01), { it.first() == 0x0C.toByte() }) {
            it.drop(2).toByteArray().toByteBuffer().uint32Le * 1000
        }
        GlobalScope.launch(ApplicationDispatcher) {
            try {
                val date = notepadType!!.executeCommand(command)
                success(date)
            } catch (e: Exception) {
                error(Failure(e))
            }
        }
    }

    // to: millis
    override fun setDeviceDate(to: Long, complete: () -> Unit, error: (Failure) -> Unit) {
        val seconds = to.toUInt() / 1000U
        val command = WoodemiCommand(byteArrayOf(0x0A, *seconds.toInt().toByteArray()), handle = WoodemiCommand.defaultHandle)
        GlobalScope.launch(ApplicationDispatcher) {
            try {
                notepadType!!.executeCommand(command)
                complete()
            } catch (e: Exception) {
                error(Failure(e))
            }
        }
    }

    override fun getAutoLockTime(success: (Int) -> Unit, error: (Failure) -> Unit) {
        val command = WoodemiCommand(byteArrayOf(0x08, 0x05), { it.first() == 0x10.toByte() }) {
            //  固件版本号：<=1.1.1的，返回只有5个字节，需要补一个字节长度的数据:0x00。此方法对版本号>1.1.1的无不良影响。
            (it.drop(2) + 0x00).toByteBuffer().uint32Le.toInt()
        }
        GlobalScope.launch(ApplicationDispatcher) {
            try {
                val time = notepadType!!.executeCommand(command)
                success(time)
            } catch (e: Exception) {
                error(Failure(e))
            }
        }
    }

    // to: seconds
    override fun setAutoLockTime(to: Int, complete: () -> Unit, error: (Failure) -> Unit) {
        val command = WoodemiCommand(byteArrayOf(0x11, 0x01, *to.toByteArray()), handle = WoodemiCommand.defaultHandle)
        GlobalScope.launch(ApplicationDispatcher) {
            try {
                notepadType!!.executeCommand(command)
                complete()
            } catch (e: Exception) {
                error(Failure(e))
            }
        }
    }

    override fun getVersionInfo(success: (VersionInfo) -> Unit, error: (Failure) -> Unit) {
        val command = WoodemiCommand(byteArrayOf(0x08, 0x00), { it.first() == 0x09.toByte() }) {
            val buffer = it.drop(1).toByteBuffer()
            val hardwareType = buffer.uint8
            val hardware = Version(buffer.uint8.toInt(), buffer.uint8.toInt())
            val software = Version(buffer.uint8.toInt(), buffer.uint8.toInt(), buffer.uint8.toInt())
            VersionInfo(hardware, software)
        }
        GlobalScope.launch(ApplicationDispatcher) {
            try {
                val versionInfo = notepadType!!.executeCommand(command)
                success(versionInfo)
            } catch (e: Exception) {
                error(Failure(e))
            }
        }
    }

    override fun setMode(to: NotepadMode, complete: () -> Unit, error: (Failure) -> Unit) {
        val (request, priority) = when (to) {
            NotepadMode.Sync -> byteArrayOf(0x05, 0x00) to BleConnectionPriority.High
            NotepadMode.Common -> byteArrayOf(0x05, 0x01) to BleConnectionPriority.Balanced
        }
        GlobalScope.launch(ApplicationDispatcher) {
            try {
                notepadType!!.executeCommand(WoodemiCommand(request, handle = WoodemiCommand.defaultHandle))
                notepadType!!.requestConnectionPriority(priority)
                complete()
            } catch (e: Exception) {
                error(Failure(e))
            }
        }
    }

    override fun parseSyncData(bytes: ByteArray): Array<NotePenPointer> {
        return NotePenPointer.create(bytes).map {
            NotePenPointer(
                ((client.originX + it.x).toDouble() * client.sizeScale).toInt(),
                ((client.originX + it.y).toDouble() * client.sizeScale).toInt(),
                it.t,
                (it.p * client.pressureScale).toInt())
        }.filter {
            it.x in 0..client.width && it.y in 0..client.height
        }.toTypedArray()
    }

    override fun getMemoSummary(success: (MemoSummary) -> Unit, error: (Failure) -> Unit) {
        val command = WoodemiCommand(byteArrayOf(0x08, 0x02), { it.first() == 0x0D.toByte() }) {
            var data = it.drop(1).toByteBuffer()
            val totalCapacity = data.uint32Le
            val freeCapacity = data.uint32Le
            val usedCapacity = data.uint32Le
            val memoCount = data.uint16Le
            MemoSummary(memoCount, totalCapacity, freeCapacity, usedCapacity)
        }
        GlobalScope.launch(ApplicationDispatcher) {
            try {
                val memoSummary = notepadType!!.executeCommand(command)
                success(memoSummary)
            } catch (e: Exception) {
                error(Failure(e))
            }
        }
    }

    override fun getMemoInfo(success: (MemoInfo) -> Unit, error: (Failure) -> Unit) {
        GlobalScope.launch(ApplicationDispatcher) {
            try {
                val largeDataInfo = getLargeDataInfo()
                val memoInfo = MemoInfo(
                        largeDataInfo.sizeInByte - ImageTransportation.EMPTY_LENGTH,
                    largeDataInfo.createdAt,
                    largeDataInfo.partIndex,
                    largeDataInfo.restCount
                )
                success(memoInfo)
            } catch (e: Exception) {
                error(e)
            }
        }
    }

    private suspend fun getLargeDataInfo(): MemoInfo {
        val data = fileInfo.imageId + fileInfo.imageVersion
        val command = WoodemiCommand(byteArrayOf(0x02, *data), { it.first() == 0x03.toByte() }) {
            var data = it.drop(1).toByteBuffer()

            val partIndex = data.uint8.toInt()
            val restCount = data.uint8.toInt()

            val chars = ByteArray(fileInfo.imageVersion.size) { data.get() }.map { it.toChar() }
            val createdAt = String(chars.toCharArray()).toLong(16) * 1000

            val sizeInByte = data.uint32Le

            MemoInfo(sizeInByte, createdAt, partIndex, restCount)
        }
        return notepadType!!.executeFileInputControl(command)
    }

    private val fileInfo = object {
        val imageId = byteArrayOf(0x00, 0x01)
        val imageVersion = byteArrayOf(
            0x01, 0x00, 0x00, // Build Version
            0x41, // Stack Version
            0x11, 0x11, 0x11, // Hardware Id
            0x01 // Manufacturer Id
        )
    }

    /**
     * Memo is kind of LargeData, transferred in data structure [ImageTransportation]
     * +----------------------------------------------------------------+
     * |                            LargeData                           |
     * +--------------------------+----------+--------------------------+
     * | [ImageTransportation]    |   ...    |  [ImageTransportation]   |
     * +--------------------------+----------+--------------------------+
     */
    override fun importMemo(progress: (Int) -> Unit, success: (MemoData) -> Unit, error: (Failure) -> Unit) {
        GlobalScope.launch(ApplicationDispatcher) {
            try {
                val info = getLargeDataInfo()
                if (info.sizeInByte <= ImageTransportation.EMPTY_LENGTH) throw Exception("No memo")

                // TODO LargeData with multiple ImageTransportation
                val imageData = requestTransportation(info.sizeInByte, progress)
                success(MemoData(info, parseMemo(imageData, info.createdAt, client)))
            } catch (e: Exception) {
                error(Failure(e))
            }
        }
    }

    /**
     * +-----------------------------------+
     * |       [ImageTransportation]       |
     * +-----------+-----------+-----------+
     * |  block    |    ...    |   block   |
     * +-----------+-----------+-----------+
     */
    private suspend fun requestTransportation(totalSize: Long, progress: (Int) -> Unit): ByteArray {
        var data = byteArrayOf()
        while (data.size < totalSize) {
            val currentPos = data.size
            var blockOffset = 0
            val blockChunkMap = requestForNextBlock(currentPos, totalSize).fold(mapOf<Int, ByteArray>()) { acc, value ->
                blockOffset += value.second.size
                progress(((currentPos + blockOffset) * 100 / totalSize).toInt())
                acc.plus(value)
            }
            val block = blockChunkMap.keys.sorted().map { blockChunkMap[it]!! }.reduce { acc, bytes -> acc + bytes }
            data += block.also { Logger.d(TAG, "receiveBlock size(${it.size})") }
        }

        return ImageTransportation(data).imageData
    }

    /**
     * Request in file input control pipe
     * +------------+--------------------------------------------------------------------------------------------+
     * | requestTag |                                     requestData                                            |
     * +------------+----------+-------------+------------+---------------+-----------------+--------------------+
     * |            |  imageId |  currentPos |  BlockSize |  maxChunkSize |  transferMethod |  l2capChannelOrPsm |
     * |            |          |             |            |               |                 |                    |
     * | 1 byte     |  2 bytes |  4 bytes    |  4 bytes   |  2bytes       |  1 byte         |  2 bytes           |
     * +------------+----------+-------------+------------+---------------+-----------------+--------------------+
     *
     * [maxChunkSize] not larger than (0xFFFF + 1)
     *
     * Response in file input data pipe
     * +--------------------------------+
     * |             block              |
     * +----------+----------+----------+
     * | chunk    |   ...    |  chunk   |
     * +----------+----------+----------+
     */
    private suspend fun requestForNextBlock(currentPos: Int, totalSize: Long): Flow<Pair<Int, ByteArray>> {
        val maxChunkSize = notepadType!!.mtu - GATT_HEADER_LENGTH - 1 /*responseTag*/ - 1 /*chunkSeqId*/
        val maxBlockSize = maxChunkSize.toLong() * (0xFF + 1) /*chunkSeqId(1 byte) -> maxChunkPerBlock*/
        val blockSize = min(totalSize - currentPos, maxBlockSize)
        val transferMethod: Byte = 0
        val l2capChannelOrPsm: Short = 0x0004

        Logger.d(
            TAG,
            "requestForNextBlock currentPos $currentPos, totalSize $totalSize, blockSize, $blockSize, maxChunkSize $maxChunkSize"
        )
        val requestData = (fileInfo.imageId
                + currentPos.toByteArray() + blockSize.toInt().toByteArray()
                + maxChunkSize.toShort().toByteArray()
                + byteArrayOf(transferMethod) + l2capChannelOrPsm.toByteArray())

        val chunkCountCeil = ceil(blockSize.toDouble() / maxChunkSize).toInt()
        val indexedChunkFlow = receiveChunks(chunkCountCeil)

        notepadType!!.sendRequestAsync(
            "FileInputControl",
            fileInputControlRequestCharacteristic,
            byteArrayOf(0x04, *requestData)
        )

        return indexedChunkFlow
    }

    /**
     * +-------------+--------------------------+
     * | responseTag |       responseData       |
     * +-------------+-------------+------------+
     * |             |  chunkSeqId |  chunkData |
     * |             |             |            |
     * | 1 byte      |  1 byte     |  ...       |
     * +-------------+-------------+------------+
     */
    private fun receiveChunks(count: Int) = notepadType!!.receiveFileInput()
        .filter { it.first() == 0x05.toByte() }
        // TODO Take with timeout
        .take(count)
        // TODO Check chunkSeqId
        .map { it[1].toUByte().toInt() to it.drop(2).toByteArray() }

    override fun deleteMemo(complete: () -> Unit, error: (Failure) -> Unit) {
        // TODO Deal with 0x01 as notification instead of response
        GlobalScope.launch(ApplicationDispatcher) {
            try {
                val request = notepadType!!.sendRequestAsync("FileInputControl", fileInputControlRequestCharacteristic,
                        byteArrayOf(0x06, 0x00, 0x00, 0x00))
                request.join()
                complete()
            } catch (e: Exception) {
                error(Failure(e))
            }
        }
    }

    private var upgradeIterator: ChunkIterator? = null

    @ExperimentalStdlibApi
    override fun upgrade(with: String, version: Version, progress: (Int) -> Unit, complete: () -> Unit, error: (Failure) -> Unit) {
        val (imageId, imageVersion, imageData) = parseUpgradeFile(with).let {
            ImageTransportation.build(it, version).run { Triple(imageId, imageVersion, toByteArray()) }
        }
        GlobalScope.launch(ApplicationDispatcher) {
            try {
                notepadType!!.sendRequestAsync("FileOutputControl", fileOutputControlRequestCharacteristic,
                        byteArrayOf(0x03) + imageId + imageVersion + imageData.size.toByteArray())
                loop@ while (true) {
                    val receivedRequest = notepadType!!.receiveResponseAsync("FileOutputControl", fileOutputControlResponseCharacteristic) { true }.await()
                    when (receivedRequest.first()) {
                        0x04.toByte() -> {
                            if (upgradeIterator != null) continue@loop
                            upgradeIterator = ChunkIterator.fromRequest(receivedRequest).also {
                                sendChunksRecursively(imageData, it, progress)
                            }
                        }
                        0x06.toByte() -> if (receivedRequest[3] == 0x00.toByte()) break@loop else throw AssertionError("Data CRC fail")
                        else -> Logger.d(TAG, "Unknown receivedRequest ${receivedRequest.contentToString()}")
                    }
                }
                upgradeIterator = null
                complete()
            } catch (e: Exception) {
                error(Failure(e))
            }
        }
    }

    private class ChunkIterator(val currentPos: Int, val blockSize: Int, val maxChunkSize: Int) : Iterator<Pair<Int, IntRange>> {
        companion object {
            fun fromRequest(request: ByteArray): ChunkIterator {
                val buffer = request.drop(1).toByteBuffer()
                val imageId = ByteArray(2).also { buffer.read(it) }
                val currentPos = buffer.uint32Le.toInt()
                val blockSize = buffer.uint32Le.toInt()
                val maxChunkSize = buffer.uint16Le
                val transferMethod = buffer.uint8
                val l2capChannelOrPsm = buffer.uint16Le
                Logger.i("ChunkIterator", "ChunkIterator.fromRequest imageId ${imageId.contentToString()}, currentPos $currentPos, blockSize $blockSize, maxChunkSize $maxChunkSize")

                return ChunkIterator(currentPos, blockSize, maxChunkSize)
            }
        }

        private val chunkCount = ceil(blockSize.toDouble() / maxChunkSize).toInt()
        private var chunkIndex = 0

        override fun hasNext() = chunkIndex < chunkCount

        override fun next(): Pair<Int, IntRange> {
            val start = chunkIndex * maxChunkSize
            val chunkSize = min(blockSize - start, maxChunkSize)
            val totalDataRange = currentPos + start until currentPos + start + chunkSize
            return (chunkIndex to totalDataRange).also {
                chunkIndex++
            }
        }
    }

    private fun sendChunksRecursively(totalData: ByteArray, chunkIterator: ChunkIterator, progress: (Int) -> Unit) {
        GlobalScope.launch(ApplicationDispatcher) {
            val (chunkIndex, totalDataRange) = chunkIterator.next()
            Logger.d(TAG, "sendChunksRecursively $chunkIndex, $totalDataRange")
            val chunk = byteArrayOf(0x05, chunkIndex.toByte()) + totalData.sliceArray(totalDataRange)
            notepadType!!.sendValue(fileOutputCharacteristic, chunk, BleOutputProperty.WithoutResponse)
            progress(100 * totalDataRange.last / totalData.size)

            if (chunkIterator.hasNext()) {
                dispatchAfter(DEVICE_PROCESSING_INTERVAL.toLong()) { sendChunksRecursively(totalData, chunkIterator, progress) }
            } else {
                upgradeIterator = null
            }
        }
    }
}