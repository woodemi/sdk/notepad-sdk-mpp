package io.woodemi.notepad.woodemi

import io.woodemi.notepad.NotepadCommand

/**
 * Created by wangkun on 2018/6/7.
 */

private const val TAG__RESP_COMMON: Byte = 0x07
private const val VALUE__RESP_SUCCESS: Byte = 0

internal class WoodemiCommand<Response>(
        request: ByteArray,
        intercept: (ByteArray) -> Boolean = { it.first() == TAG__RESP_COMMON && it[1] == request.first() },
        handle: (ByteArray) -> Response
) : NotepadCommand<Response>(request, intercept, handle) {
    companion object {
        val defaultHandle: (ByteArray) -> Unit = {
            if (it[4] != VALUE__RESP_SUCCESS) throw Exception("WOODEMI_COMMAND fail: response ${it.contentToString()}")
        }
    }
}