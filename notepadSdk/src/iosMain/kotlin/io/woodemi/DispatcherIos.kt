package io.woodemi

import kotlinx.coroutines.CoroutineDispatcher
import kotlinx.coroutines.Runnable
import platform.darwin.*
import kotlin.coroutines.CoroutineContext

internal actual val ApplicationDispatcher: CoroutineDispatcher = MainDispatcher

private object MainDispatcher : CoroutineDispatcher() {
    override fun dispatch(context: CoroutineContext, block: Runnable) {
        dispatch_async(dispatch_get_main_queue()) {
            block.run()
        }
    }
}

internal actual suspend fun dispatchAfter(timeMillis: Long, block: () -> Unit) {
    val nanos = timeMillis.toULong() * NSEC_PER_MSEC
    val time = dispatch_time(DISPATCH_TIME_NOW, nanos.toLong())
    dispatch_after(time, dispatch_get_main_queue(), block)
}