package io.woodemi

import platform.Foundation.*

/**
 * Created by wangkun on 2019-05-09.
 */
actual object Logger {
    actual var isEnabled = true

    private val formatter = NSDateFormatter().apply {
        dateFormat = "yyyy-MM-dd hh:mm:ssSSS"
        locale = NSLocale.currentLocale
        timeZone = NSTimeZone.localTimeZone
    }

    private fun NSDate.Companion.now() = formatter.stringFromDate(NSDate())

    /**
     * Verbose
     */
    actual fun v(tag: String, message: String) {
        if (!isEnabled) return
        println("${NSDate.now()} V/$tag: $message")
    }

    /**
     * Debug
     */
    actual fun d(tag: String, message: String) {
        if (!isEnabled) return
        println("${NSDate.now()} D/$tag: $message")
    }

    /**
     * Info
     */
    actual fun i(tag: String, message: String) {
        if (!isEnabled) return
        println("${NSDate.now()} I/$tag: $message")
    }

    /**
     * Warning
     */
    actual fun w(tag: String, message: String) {
        if (!isEnabled) return
        println("${NSDate.now()} W/$tag: $message")
    }

    /**
     * Error
     */
    actual fun e(tag: String, message: String) {
        if (!isEnabled) return
        println("${NSDate.now()} E/$tag: $message")
    }

    /**
     * Severe
     */
    actual fun s(tag: String, message: String) {
        if (!isEnabled) return
        println("${NSDate.now()} S/$tag: $message")
    }
}