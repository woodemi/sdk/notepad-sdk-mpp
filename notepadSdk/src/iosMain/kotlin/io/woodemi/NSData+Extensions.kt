package io.woodemi.notepad

import kotlinx.cinterop.memScoped
import kotlinx.cinterop.refTo
import kotlinx.cinterop.toCValues
import platform.Foundation.NSData
import platform.Foundation.create
import platform.posix.memcpy

/**
 * Created by wangkun on 2019-04-17.
 */
fun ByteArray.toNSData(): NSData = memScoped {
    NSData.create(bytes = toCValues().getPointer(this), length = size.toULong())
}

fun NSData.toByteArray(): ByteArray = memScoped {
        if (length.toInt() == 0) ByteArray(0)
        else ByteArray(length.toInt()).also {
            memcpy(it.refTo(0), bytes, length)
        }
    }