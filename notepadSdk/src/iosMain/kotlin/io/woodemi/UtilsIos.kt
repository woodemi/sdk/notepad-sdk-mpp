package io.woodemi

import platform.Foundation.NSString
import platform.Foundation.NSUTF8StringEncoding
import platform.Foundation.stringWithContentsOfFile

/**
 * Created by wangkun on 2019-06-27.
 */
// FIXME error null
actual fun readUTF8Text(path: String): String =
        NSString.stringWithContentsOfFile(path, encoding = NSUTF8StringEncoding, error = null)!!