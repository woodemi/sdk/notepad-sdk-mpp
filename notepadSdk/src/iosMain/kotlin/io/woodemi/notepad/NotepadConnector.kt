package io.woodemi.notepad

import io.woodemi.ApplicationDispatcher
import io.woodemi.Logger
import kotlinx.coroutines.GlobalScope
import kotlinx.coroutines.channels.Channel
import kotlinx.coroutines.launch
import platform.CoreBluetooth.*
import platform.Foundation.NSData
import platform.Foundation.NSError
import platform.Foundation.NSNumber
import platform.darwin.NSObject

/**
 * Created by wangkun on 2019-04-17.
 */
interface NotepadScanDelegate {
    fun didChange(state: Boolean)
    fun didDiscover(notepadScanResult: NotepadScanResult)
}

interface NotepadConnectionDelegate {
    fun willConnect(notepadClient: NotepadClient)
    fun awaitConfirm(notepadClient: NotepadClient)
    fun didConnect(notepadClient: NotepadClient)
    fun didDisconnect(notepadClient: NotepadClient, cause: DisconnectionCause = DisconnectionCause.Unknown)
}

private const val TAG = "NotepadConnector"

object NotepadConnector {
    private val managerDelegate = CentralManagerDelegate()
    private val manager = CBCentralManager(managerDelegate, null)

    // [NotepadConnector] is object in kotlin so as immutable in swift/objc
    var scanDelegate: NotepadScanDelegate?
        get() = managerDelegate.scanDelegate
        set(value) {
            managerDelegate.scanDelegate = value
        }

    val isEnabled: Boolean
        get() = manager.state == CBCentralManagerStatePoweredOn

    fun startScan() {
        Logger.i(TAG, "startScan $isEnabled")
        if (!isEnabled) return

        manager.scanForPeripheralsWithServices(null, null)
    }

    fun stopScan() {
        Logger.i(TAG, "stopScan $isEnabled")
        if (!isEnabled || !manager.isScanning) return

        manager.stopScan()
    }

    // [NotepadConnector] is object in kotlin so as immutable in swift/objc
    var connectionDelegate: NotepadConnectionDelegate?
        get() = managerDelegate.connectionDelegate
        set(value) {
            managerDelegate.connectionDelegate = value
        }

    fun connect(to: NotepadScanResult, authToken: ByteArray? = null) {
        Logger.i(TAG, "connect")
        val peripheral = managerDelegate.peripherals[to.deviceId]
                ?: throw Exception("Unavailable peripheral")
        managerDelegate.notepadScanResult = to

        val notepadClient = NotepadHelper.create(to.manufacturerData, authToken)
        val notepadType = NotepadType(notepadClient, BleType(peripheral))
        peripheral.delegate = PeripheralDelegate(notepadType).also {
            managerDelegate.peripheralDelegate = it
        }

        manager.connectPeripheral(peripheral, null)

        connectionDelegate?.willConnect(notepadClient)
    }

    fun disconnect() {
        Logger.i(TAG, "disconnect")
        val notepadScanResult = managerDelegate.notepadScanResult ?: return
        val peripheral = managerDelegate.peripherals[notepadScanResult.deviceId] ?: return
        manager.cancelPeripheralConnection(peripheral)
    }
}

private class CentralManagerDelegate : NSObject(), CBCentralManagerDelegateProtocol {
    var scanDelegate: NotepadScanDelegate? = null

    override fun centralManagerDidUpdateState(central: CBCentralManager) {
        Logger.v(TAG, "centralManagerDidUpdateState ${central.state}")
        scanDelegate?.didChange(central.state == CBCentralManagerStatePoweredOn)
    }

    val peripherals = mutableMapOf<String, CBPeripheral>()

    override fun centralManager(central: CBCentralManager, didDiscoverPeripheral: CBPeripheral, advertisementData: Map<Any?, *>, RSSI: NSNumber) {
        Logger.v(
            TAG,
            "centralManager:didDiscoverPeripheral ${didDiscoverPeripheral.name} ${didDiscoverPeripheral.identifier}"
        )
        val manufacturerData = advertisementData[CBAdvertisementDataManufacturerDataKey] as NSData?
        val notepadScanResult = NotepadScanResult(didDiscoverPeripheral.name, didDiscoverPeripheral.identifier.UUIDString, manufacturerData?.toByteArray(), RSSI.intValue)

        if (NotepadHelper.support(notepadScanResult.manufacturerData)) {
            peripherals[notepadScanResult.deviceId] = didDiscoverPeripheral
            scanDelegate?.didDiscover(notepadScanResult)
        }
    }

    var notepadScanResult: NotepadScanResult? = null

    var connectionDelegate: NotepadConnectionDelegate? = null

    var peripheralDelegate: PeripheralDelegate? = null

    override fun centralManager(central: CBCentralManager, didConnectPeripheral: CBPeripheral) {
        Logger.v(TAG, "centralManager:didConnect ${didConnectPeripheral.identifier}")
        if (didConnectPeripheral.identifier.UUIDString != notepadScanResult?.deviceId) return

        val peripheralDelegate = peripheralDelegate ?: return
        val notepadType = peripheralDelegate.notepadType
        GlobalScope.launch(ApplicationDispatcher) {
            try {
                didConnectPeripheral.discoverServices(peripheralDelegate)
                notepadType.configCharacteristics()
                notepadType.notepadClient.completeConnection { awaitConfirm: Boolean ->
                    connectionDelegate?.awaitConfirm(notepadType.notepadClient)
                }
                connectionDelegate?.didConnect(notepadType.notepadClient)
            } catch (e: AccessException) {
                // FIXME [CBPeripheral:didDisconnect] prioritize [AccessException.Unconfirmed]
                val disconnectionCause = when (e) {
                    AccessException.Denied -> DisconnectionCause.Unauthorized
                    AccessException.Unconfirmed -> DisconnectionCause.Unconfirmed
                }
                connectionDelegate?.didDisconnect(notepadType.notepadClient, disconnectionCause)
            } catch (e: Exception) {
                connectionDelegate?.didDisconnect(notepadType.notepadClient)
            }
        }
    }

    override fun centralManager(central: CBCentralManager, didDisconnectPeripheral: CBPeripheral, error: NSError?) {
        Logger.v(TAG, "centralManager:didDisconnectPeripheral: $didDisconnectPeripheral error: $error")
        val peripheralDelegate = peripheralDelegate ?: return
        connectionDelegate?.didDisconnect(peripheralDelegate.notepadClient)
    }
}

@Suppress("CONFLICTING_OVERLOADS")
private class PeripheralDelegate(val notepadType: NotepadType) : NSObject(), CBPeripheralDelegateProtocol {
    val notepadClient = notepadType.notepadClient

    override fun peripheral(peripheral: CBPeripheral, didDiscoverServices: NSError?) {
        Logger.v(TAG, "peripheral: ${peripheral.identifier} didDiscoverServices: $didDiscoverServices")
        peripheral.services?.forEach {
            peripheral.discoverCharacteristics(null, it as CBService)
        }
    }

    override fun peripheral(peripheral: CBPeripheral, didDiscoverCharacteristicsForService: CBService, error: NSError?) {
        didDiscoverCharacteristicsForService.characteristics?.forEach {
            val characteristic = it as CBCharacteristic
            Logger.v(TAG, "peripheral:didDiscoverCharacteristicsForService ${characteristic.serviceCharacteristic}")
        }
        GlobalScope.launch(ApplicationDispatcher) {
            serviceConfigChannel.send(didDiscoverCharacteristicsForService.UUID.UUIDString)
        }
    }

    private val serviceConfigChannel = Channel<String>()

    suspend fun confirmInputServices() {
        Logger.d(TAG, "confirmInputServices ${notepadClient.inputServices}")
        val mutableList = notepadClient.inputServices.toMutableList()
        while (mutableList.size > 0) {
            mutableList.remove(serviceConfigChannel.receive())
        }
    }

    override fun peripheral(peripheral: CBPeripheral, didUpdateNotificationStateForCharacteristic: CBCharacteristic, error: NSError?) {
        Logger.v(
            TAG,
            "peripheral:didUpdateNotificationStateFor ${with(didUpdateNotificationStateForCharacteristic) { "$UUID $isNotifying $isBroadcasted" }}"
        )
        GlobalScope.launch(ApplicationDispatcher) {
            notepadType.characteristicConfigChannel.send(didUpdateNotificationStateForCharacteristic.serviceCharacteristic)
        }
    }

    override fun peripheral(peripheral: CBPeripheral, didWriteValueForCharacteristic: CBCharacteristic, error: NSError?) {
        val characteristicValue = didWriteValueForCharacteristic.characteristicValue
        Logger.v(
            TAG,
            "peripheral:didWriteValueForCharacteristic ${characteristicValue.first} ${characteristicValue.second.contentToString()} error: $error"
        )
        GlobalScope.launch(ApplicationDispatcher) { notepadType.outputValueChannel.send(characteristicValue) }
    }

    override fun peripheral(peripheral: CBPeripheral, didUpdateValueForCharacteristic: CBCharacteristic, error: NSError?) {
        val characteristicValue = didUpdateValueForCharacteristic.characteristicValue
        Logger.v(
            TAG,
            "peripheral:didUpdateValueForCharacteristic ${characteristicValue.first} ${characteristicValue.second.contentToString()} error: $error"
        )
        GlobalScope.launch(ApplicationDispatcher) { notepadType.inputValueChannel.send(characteristicValue) }
    }
}

private val CBCharacteristic.serviceCharacteristic
    get() = service.UUID.UUIDString to UUID.UUIDString

private val CBCharacteristic.characteristicValue
    get() = UUID.UUIDString to (value?.toByteArray() ?: byteArrayOf())

private suspend fun CBPeripheral.discoverServices(peripheralDelegate: PeripheralDelegate) {
    Logger.d(TAG, "CBPeripheral.discoverServices")
    // TODO Check cache of services in notepadScanResult.peripheral
    discoverServices(null)
    peripheralDelegate.confirmInputServices()
}