package io.woodemi.notepad

/**
 * Created by wangkun on 2019/4/17.
 */
actual data class NotepadScanResult(
    actual val name: String?,
    actual val deviceId: String,
    actual val manufacturerData: ByteArray?,
    actual val rssi: Int
)