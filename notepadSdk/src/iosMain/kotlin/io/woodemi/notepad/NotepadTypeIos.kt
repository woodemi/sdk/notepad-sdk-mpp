package io.woodemi.notepad

import io.woodemi.ApplicationDispatcher
import kotlinx.coroutines.GlobalScope
import kotlinx.coroutines.channels.Channel
import kotlinx.coroutines.launch
import platform.CoreBluetooth.*

/**
 * Created by wangkun on 2019-04-26.
 */
internal actual class BleType(private val peripheral: CBPeripheral) {
    actual val mtuConfigChannel: Channel<Int> = Channel()

    actual fun requestMtu(expectedMtu: Int) {
        val maxLength = peripheral.maximumWriteValueLengthForType(CBCharacteristicWriteWithoutResponse)
        GlobalScope.launch(ApplicationDispatcher) {
            mtuConfigChannel.send(maxLength.toInt() + GATT_HEADER_LENGTH)
        }
    }

    actual fun requestConnectionPriority(priority: BleConnectionPriority) {
        // Empty by design
    }

    actual fun setNotifiable(serviceCharacteristic: Pair<String, String>, inputProperty: BleInputProperty) {
        val characteristic = peripheral.getCharacteristic(serviceCharacteristic)
            ?: throw Exception("$serviceCharacteristic setNotifiable fail")
        peripheral.setNotifyValue(true, characteristic)
    }

    actual fun readValue(serviceCharacteristic: Pair<String, String>) {
        val characteristic = peripheral.getCharacteristic(serviceCharacteristic)
                ?: throw Exception("$serviceCharacteristic readValue fail")

        peripheral.readValueForCharacteristic(characteristic)
    }

    actual fun writeValue(serviceCharacteristic: Pair<String, String>, value: ByteArray, outputProperty: BleOutputProperty) {
        val characteristic = peripheral.getCharacteristic(serviceCharacteristic)
                ?: throw Exception("$serviceCharacteristic writeValue fail")

        val type = when (outputProperty) {
            BleOutputProperty.WithResponse -> CBCharacteristicWriteWithResponse
            BleOutputProperty.WithoutResponse -> CBCharacteristicWriteWithoutResponse
        }
        peripheral.writeValue(value.toNSData(), characteristic, type)
    }
}

fun CBPeripheral.getCharacteristic(serviceCharacteristic: Pair<String, String>) =
        services?.filterIsInstance<CBService>()?.find {
            it.UUID == CBUUID.UUIDWithString(serviceCharacteristic.first)
        }?.characteristics?.filterIsInstance<CBCharacteristic>()?.find {
            it.UUID == CBUUID.UUIDWithString(serviceCharacteristic.second)
        }